
using System;

namespace ConDis
{
	/// <summary>
	/// Summary description for HTMLEntity.
	/// </summary>
	public class HTMLEntity
	{
		public string CharacterEntity;
		public string NumericEntity;
		public string Character;


		public HTMLEntity( string characterEntity, string numericEntity, string character )
		{
			CharacterEntity = characterEntity;
			NumericEntity = numericEntity;
			Character = character;
			
		}
	}
}

