using System;
using System.Configuration;
using System.Diagnostics;

namespace ConDis
{
	/// <summary>
	/// 
	/// </summary>
	public class Config
	{
		public Config()
		{
			// 
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		/// Gets a value for the given key from the config-file.
		/// </summary>
		public static string Get( string key )
		{
			string val = "";

            
			try
			{

                val = Config.Get(key);
				// val = ConfigurationSettings.AppSettings[ key ];

				if( val == null || val == "")
				{
                    
                    throw new ConfigurationException( "Key/value pair for " + key + " not found in configuration file.");					
				}

				return val;
			}
			catch(Exception e)
			{
				EventLog.WriteEntry(AppDomain.CurrentDomain.FriendlyName, "Unable to retrive string for '" + key + "'.\r\n" + e.ToString(), EventLogEntryType.Error);
				throw e;
			}
		}
		public static void Set( string key, string val )
		{
			try
			{
				if( key == null || key == "")
				{
					throw new ConfigurationException( "The given configuration key is empty.");					
				}

	//			ConfigurationSettings.AppSettings.Set( key, val );
			}
			catch(Exception e)
			{
				EventLog.WriteEntry(AppDomain.CurrentDomain.FriendlyName, "Unable to retrive string for '" + key + "'.\r\n" + e.ToString(), EventLogEntryType.Error);
				throw e;
			}
		}

	}
}
