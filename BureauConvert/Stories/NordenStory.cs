using System;
using System.Web;
using System.Globalization;
using System.Text.RegularExpressions;

namespace ConDis
{
	/// <summary>
	/// Klasse for de nordiske byr�ene
	/// </summary>
	public class NordenStory : ConDis.Story
	{


		public NordenStory()
		{
			//
		}

		public NordenStory( string filename, bool analyze, bool isTest, ref Configuration conf )
		{
			NotabeneSubdirectory = "Norden";
			SportsCategory = "SPO";
			EconomyCategory = "EBF";
			EditorialCategory = "RED";
			EditorialCategory2 = "XX";
			IncomingFolder = "";

			IsTest = isTest;
			FullOriginalPath = filename;
			EditedText = ReadFile( FullOriginalPath, "", false, false );

			//Extra celaning bad NTB tags
			EditedText = EditedText.Replace("#lt;","<");
			EditedText = EditedText.Replace("#gt;",">");

			// Cleaning some TT weird characters
			EditedText = EditedText.Replace("&tstr;", "&#8211;");
			
			EditedText = System.Web.HttpUtility.HtmlDecode(EditedText);
			EditedText = EditedText.Replace("<<","<");
			EditedText = EditedText.Replace(">>",">");
			
			//Remove DTD spec as the DTD ruins opening the XML with an XML parser
			EditedText = EditedText.Replace("<!DOCTYPE nornitf SYSTEM \"nornitf.dtd\">","<!--DOCTYPE nornitf SYSTEM \"nornitf.dtd\"-->");
			EditedText = EditedText.Replace("<!DOCTYPE NORNITF SYSTEM \"nornitf.dtd\">","<!--DOCTYPE NORNITF SYSTEM \"nornitf.dtd\"-->");

			//Some general cleanup
			EditedText = EditedText.Replace("<TEXT>","<TEXT>\n"); 
			EditedText = EditedText.Replace("</TEXT>","\n</TEXT>"); 

			Regex rx = new Regex("&(?!(apos|quot|#|amp))");
			EditedText = rx.Replace(EditedText,"&amp;");
			
			//rx = new Regex("<(?!/)");
			//EditedText = rx.Replace(EditedText,"\n<");

			if ( CountStrings(EditedText,"\n") <= 3 )
			{
				EditedText = EditedText.Replace("</"," \n</"); 
				EditedText = EditedText.Replace("<","\n<"); 
			}
			
//			EditedText = EditedText.Replace("</"," \n</"); 
//			EditedText = EditedText.Replace("<","\n<"); 

			#region "Disse to vaskene er tvilsomme hacks. De �delegger for tabellene fra TT og kan brekke i fremtiden"

			//FNB headers mess up
			EditedText = EditedText.Replace("<Header>",""); 
			EditedText = EditedText.Replace("</Header>",""); 
			EditedText = EditedText.Replace("<Company>",""); 
			EditedText = EditedText.Replace("</Company>",""); 

			//And Swedish foppal tables
			EditedText = EditedText.Replace("<SPLINJE LVAD=\"Annan\">",""); 

			#endregion

				 
			//Clean up the "XML" a little
			bool inHead = false;
			bool inText = false;
			int po=0, pc=0, otpos=0;

			String delim = "\r\n";
			string[] lines = EditedText.Split(delim.ToCharArray());
			for ( int i = 0; i < lines.GetLength(0); i++ )
			{
				//Skip blank lines
				if ( lines[i] == "" ) continue;

				//TRack our position in the XML an do some work at certain positions
				else if ( lines[i].StartsWith("<HEAD>") ) inHead = true;                
				else if ( lines[i].StartsWith("</HEAD>") ) inHead = false;                
				else if ( lines[i].StartsWith("<TEXT>") ) 
				{ 
					inText = true; 
					otpos = i; 
				}
				else if ( lines[i].StartsWith("</TEXT>") ) 
				{
                    if (Source != "RB")
                    {
                        if (po < pc)
                        {
                            for (int j = 0; j < (pc - po); j++)
                                lines[otpos] += "<P>";
                        }
                        else if (po > pc)
                        {
                            for (int j = 0; j < (po - pc); j++)
                                lines[i] = "</P>" + lines[i];
                        }
                        inText = false;
                    }
				}
				else if ( lines[i].StartsWith("<ORD>") ) 
				{
                    //lines[i] = lines[i].Replace("<P>","</P><P>");					
                    //lines[i] = lines[i].Replace("<ORD>","<ORD><P>");					
                    //lines[i] += "</P>";
				}
				
				//Clean up the header with close tags/quotes
				if ( inHead && (CountQuotes(lines[i]) % 2) > 0 ) lines[i] = lines[i].Replace(">","\">");
				if ( inHead && lines[i].EndsWith("\">") ) lines[i] = lines[i].Replace("\">","\" />");
			
				//Count number of opening/closing P's
				if ( inText ) 
				{
					po += CountStrings(lines[i],"<P>");	
					pc += CountStrings(lines[i],"</P>");	
				}
			}

			EditedText = "";
			for ( int i = 0; i < lines.GetLength(0); i++ )
			{
				if ( lines[i] == "" ) continue;
				EditedText += lines[i] + "\r\n";
			}

			if( analyze )
			{
				Analyze();
			}

			ExportFileName = GenerateFilename( GetNewSerialNumber( ) );
		}

		public int CountQuotes(string s)
		{
			int c = 0;
			for (int i = 0; i < s.Length ; i++)
			{
				if ( s.ToCharArray()[i].Equals('"') ) c++;
			}

			return c;
		}

		public int CountStrings(string s, string find)
		{
			int c = 0, pos = 0;
			while ( (pos = s.IndexOf(find,pos)) > -1 )
			{
				pos++;
				c++;
			}

			return c;
		}

		public string LookupSourceName(string s)
		{
			switch(s)
			{
				case "NTB":
					return "NTB";
				case "FNB":
					return "FNB";
				case "TTN":
					return "TT";
				case "RBN":
					return "RB";
				default:
					return s;
			}
		}

		public override void SetIsPrintable()
		{
			IsPrintable = false;
			try
			{
				if( (Convert.ToInt32( Priority ) < 4 ) && ( ! IsSports ) )
				{
					IsPrintable = true;
				}
			}
			catch 
			{
				IsPrintable = false;
			}

		}

		public override void GetFieldValues()
		{
			//string[] firstFields = GetFieldValue( 1, 0, "", false, false, ref Fields ).Trim().Split( ' ' );
			//string[] lastFields = GetFieldValue( 0, 0, "", true, true, ref Fields ).Trim().Split( ' ' );

			//Load the XML
			System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
			try
			{
				doc.LoadXml(EditedText);
			} 
			catch (Exception ex)
			{
				throw ex;
			}

			KeyWords			= doc.SelectSingleNode("/NORNITF/HEAD/SLUGG").InnerText.Trim();
			Source				= LookupSourceName(doc.SelectSingleNode("/NORNITF/HEAD/SERVID").InnerText.Trim());


			try
			{
				Priority			= doc.SelectSingleNode("/NORNITF/HEAD/URG/@PRIO").Value;
			}
			catch
			{
				Priority			= "5";
			}

			// NTB and FNB messages are printed out because their priority is set wrong

			// I believe this will fix the bug related to sending out alerts
			if (Source == "FNB" && Priority == "3") 
			{
				Priority = "4";
			}

			// This fixes the bug related to printing out NTB menues
			// Checking if the string contains meny so that we don't print these
			bool nottoprinter = false;
			string meny = KeyWords.ToString();
			if (meny.IndexOf("meny") != -1) 
			{
				int valMeny = meny.IndexOf("meny");
				meny = KeyWords.Substring(valMeny,4);

				if (meny == "meny") 
				{
					nottoprinter = true;
				}

			}

			string pm = KeyWords.ToString();

            // Adding length-check since FNB sometimes uses only two characters here.
            if (pm.Length > 3)
            {
                if (pm.Substring(0, 3) == "NTB")
                {
                    nottoprinter = true;
                }
            }

			// Making sure that NTB articles won't be printed!
			if (Source == "NTB") 
			{
				nottoprinter = true;
			}



			if (Source == "NTB" && nottoprinter == true) 
			{
				Priority = "5";
			}

			

			IsIgnorable = (KeyWords.ToUpper().IndexOf("LINIECHECK") >= 0);

			//Stoffgruppe
			Category			= doc.SelectSingleNode("/NORNITF/HEAD/PRODID/@TKOD").InnerText.Trim();
			if ( Category == "SPT") 
			{
				Category= "SPO";
				Priority = "5";
				nottoprinter = true;
			}

			if ( Category == "SPR" ) 
			{
				Category = "SPO";
				Priority = "5";
				nottoprinter = true;
			}
						
			ServID				= doc.SelectSingleNode("/NORNITF/HEAD/SERVID").InnerText.Trim();

			SeqNo				= doc.SelectSingleNode("/NORNITF/HEAD/NR").InnerText.Trim();
			
			Date				= doc.SelectSingleNode("/NORNITF/HEAD/DATESENT").InnerText.Trim();
			Time				= doc.SelectSingleNode("/NORNITF/HEAD/TIMESENT").InnerText.Trim();

			DateTime dt = DateTime.Now;
			if ( Date != "" && Time != "")
				dt = DateTime.ParseExact(Date + "T" + Time.Substring(0,6),"yyyyMMddTHHmmss",CultureInfo.InvariantCulture);
				
			Date				= dt.ToString("yyyy-MM-dd");
			Time				= dt.ToString("HH.mm");

			try
			{
				Header				= doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/HEADLINE").InnerText.Trim();
			} 
			catch
			{
				Header = " > Ingen overskrift < ";
			}

			
			//Lead and to-red
			if ( doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/LEAD") != null )
			{
				System.Xml.XmlNodeList nodes = doc.SelectNodes("/NORNITF/BODY/ALLTEXT/LEAD/P");
				if ( nodes.Count > 1 )
				{
					StoryField = "<P><i>" + nodes[0].InnerText.Trim() + "</i></P>";
					StoryField += "<P><B>" + nodes[1].InnerText.Trim() + "</B></P>";
				}
				else 
				{
					StoryField = "<P><B>" + doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/LEAD").InnerText.Trim() + "</B></P>";
				}

				// Get quotes in Lead
				System.Xml.XmlNodeList qnodes = doc.SelectNodes("/NORNITF/BODY/ALLTEXT/LEAD/QUOTE");
				if ( qnodes.Count > 1 )
				{
					StoryField = "<P><i>" + nodes[0].InnerText.Trim() + "</i></P>";
					StoryField += "<P><B>-" + nodes[1].InnerText.Trim() + "</B></P>";
				}
				else if (qnodes.Count == 1) 
				{
					StoryField = "<P><B>" + doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/LEAD/QUOTE").InnerText.Trim() + "</B></P>";
				}



				StoryFieldDoc = doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/LEAD").InnerText.Trim();
            }
            else
            {
                // this is a TT/FNB-fix to create a lead
                if (doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/DATELINE") != null)
                {

                    StoryField = "<P>" + doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/DATELINE/PLACE").InnerText.Trim();
                    StoryField += " (" + doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/DATELINE/SOURCE").InnerText.Trim() + "): ";
                    StoryFieldDoc = doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/DATELINE/PLACE").InnerText.Trim() + " ";
                    StoryFieldDoc += "(" + doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/DATELINE/SOURCE").InnerText.Trim() + "): ";
                }
                else if (doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/TEXT/DAT") != null) 
                {
                    StoryField = "<P>" + doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/TEXT/DAT/CITY").InnerText.Trim();
                    StoryField += " (" + doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/TEXT/DAT/SOURCE").InnerText.Trim() + "): ";
                    StoryFieldDoc = doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/TEXT/DAT/CITY").InnerText.Trim() + " ";
                    StoryFieldDoc += "(" + doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/TEXT/DAT/SOURCE").InnerText.Trim() + "): ";
                }
            }

			// QUOTE
			// if ( doc.SelectSingleNode("/nitf/body/body.content/block") != null ) 
			/*
			 * Filer som feiler slutter slik:
</P><QUOTE>F&ouml;rmodligen inte. Jag litar p&aring; referaten, s&auml;ger utrikesministern.</QUOTE></TEXT></ALLTEXT></BODY><ENDSIZE

			*/

			



			// Ritzau fix
			if ( doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/TEXT/ORD") != null ) 
				
			{
				System.Xml.XmlNodeList nodes = doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/TEXT/ORD").ChildNodes;
				foreach(System.Xml.XmlNode node in nodes)
				{
					if(node.Name == "P")
					{
						StoryField			+= "<P>" + node.InnerXml.Trim() + "</P>"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerXml.Trim();
						StoryFieldDoc		+= node.InnerText.Trim() + "\r\n"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerText.Trim();
					}
					if(node.Name == "table")
					{
						StoryField			+= "<table>" + node.InnerXml.Trim() + "</table>"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerXml.Trim();
						StoryFieldDoc		+= node.InnerText.Trim() + "\r\n"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerText.Trim();
					}

					if(node.Name == "TABLE")
					{
						StoryField			+= "<TABLE>" + node.InnerXml.Trim() + "</TABLE>"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerXml.Trim();
						// StoryFieldDoc		+= node.InnerText.Trim() + "\t"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerText.Trim();
						string StoryTemp = node.InnerXml.Trim();
						StoryTemp = StoryTemp.Replace("<TR>", "");
						StoryTemp =StoryTemp.Replace("</TR>", "\r\n");
						StoryTemp =StoryTemp.Replace("<TD>","");
						StoryTemp =StoryTemp.Replace("</TD>", "\t");
						StoryFieldDoc += StoryTemp;
					}

					// @todo: If the &#150 does not work, try &#8211;
					if(node.Name == "QUOTE")
					{
						StoryField			+= "<P>- " + node.InnerXml.Trim() + "</P>"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerXml.Trim();
						StoryFieldDoc		+= "- " + node.InnerText.Trim() + "\r\n"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerText.Trim();
					}
				}
				
			
				//				StoryField			+= doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/TEXT/ORD").InnerXml.Trim();
				//				// 						StoryFieldDoc		+= node.InnerText.Trim() + "\r\n"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerText.Trim();
				//				StoryFieldDoc		+= doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/TEXT/ORD").InnerText.Trim();
			}
				// FNB and NTB Fix
			else if ( doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/TEXT") != null ) 
			{
				System.Xml.XmlNodeList nodes = doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/TEXT").ChildNodes;
				foreach(System.Xml.XmlNode node in nodes)
				{
					if(node.Name == "P")
					{
						StoryField			+= "<P>" + node.InnerXml.Trim() + "</P>"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerXml.Trim();
						StoryFieldDoc		+= node.InnerText.Trim() + "\r\n"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerText.Trim();
					}

					// QUOTE-fix
					if(node.Name == "QUOTE")
					{
						StoryField			+= "<P>- " + node.InnerXml.Trim() + "</P>"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerXml.Trim();
						StoryFieldDoc		+= "- " + node.InnerText.Trim() + "\r\n"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerText.Trim();
					}


					if(node.Name == "HEADER")
					{
						StoryField			+= "<P><b>" + node.InnerXml.Trim() + "</b></P>"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerXml.Trim();
						StoryFieldDoc		+= node.InnerText.Trim() + "\r\n"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerText.Trim();
					}

//					if(node.Name == "TABLE")
//					{
//						StoryField			+= "<TABLE>" + node.InnerXml.Trim() + "</TABLE>"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerXml.Trim();
//						StoryFieldDoc		+= node.InnerText.Trim() + "\r\n"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerText.Trim();
//					}
					if(node.Name == "TABLE")
					{
						StoryField			+= "<TABLE>" + node.InnerXml.Trim() + "</TABLE>"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerXml.Trim();
						
						// The problem with the live below is that it takes any tag and removes them...
						string StoryTemp = node.InnerXml.Trim();
						StoryTemp = StoryTemp.Replace("<TR>", "");
						StoryTemp = StoryTemp.Replace("</TR>", "\r\n");
						StoryTemp = StoryTemp.Replace("<TD>","");
						StoryTemp = StoryTemp.Replace("</TD>", "\t");
						StoryFieldDoc += StoryTemp;
						// StoryFieldDoc		+= node.InnerText.Trim() + "\t"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerText.Trim();
					}


				
				}
			} 
			else 
			{
				try
				{
					StoryField			+= doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/TEXT").InnerXml.Trim();
					StoryFieldDoc		+= doc.SelectSingleNode("/NORNITF/BODY/ALLTEXT/TEXT").InnerText.Trim();
				} 
				catch
				{
					StoryField			+= doc.SelectSingleNode("/NORNITF/BODY").InnerXml.Trim();
					StoryFieldDoc		+= doc.SelectSingleNode("/NORNITF/BODY").InnerText.Trim();
				}
			}
		}
	}
}
