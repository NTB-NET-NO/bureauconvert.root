using System;
using System.Web;

namespace ConDis
{
	/// <summary>
	/// Klasse for byr�et AFP
	/// </summary>
	public class AFPStory : ConDis.Story
	{
		public AFPStory()
		{
			//
		}

		public AFPStory( string filename, bool analyze, bool isTest, ref Configuration conf )
		{
			NotabeneSubdirectory = "AFP";
			SportsCategory = "s*";
			EconomyCategory = "*f*";
			EditorialCategory = "n";
			EditorialCategory2 = "n";

			IsTest = isTest;
			FullOriginalPath = filename;
			EditedText = ReadFile( FullOriginalPath, "", true, true );

			if( analyze )
			{
				Analyze();
			}

            ExportFileName = GenerateFilename(GetNewSerialNumber());
		}

		public override void SetIsPrintable()
		{
			IsPrintable = false;
			string printToken = "urgent";

			if( ( Convert.ToInt32( Priority ) < 3 ) && ( ! IsSports ) )
			{
				IsPrintable = true;
			}
			else if( Header.Length >= printToken.Length )
			{
				if( Header.ToLower().Substring( 0, printToken.Length ) == printToken )
				{
					IsPrintable = true;
				}
			}
		}

		public override void GetFieldValues()
		{
			string[] firstFields = GetFieldValue( 1, 0, "", false, false, ref Fields ).Trim().Split( ' ' );
			string[] lastFields = GetFieldValue( 0, 0, "", true, true, ref Fields ).Trim().Split( ' ' );

			Priority			= firstFields[ 1 ].Trim();
			int start = 0;
			int end = 0;
			if(GetFieldValue(2, 0, "", false, false, ref Fields ).Trim() == "ALERT �")
			{
				KeyWords			= "";
				Header				= GetFieldValue( 2, 0, "", false, false, ref Fields );
				Header				= Header.Replace( "�", "" );
				GetFieldValue( 3, 0, "", false, false, ref Fields );
				start = LastFieldGotten;
				GetFieldValue( 1, 0, "", true, true, ref Fields );
				end = LastFieldGotten;
			}
			else
			{
				KeyWords			= GetFieldValue( 2, 0, "", false, false, ref Fields ).Trim();
				Header				= GetFieldValue( 3, 0, "", false, false, ref Fields );
				Header				= Header.Replace( "�", "" );
				GetFieldValue( 4, 0, "", false, false, ref Fields );
				start = LastFieldGotten;
				GetFieldValue( 1, 0, "", true, true, ref Fields );
				end = LastFieldGotten;
			}
			Source				= "AFP"; //lastFields[ 0 ].Trim(); // ( GetFieldValue( 0, 0, "", true, true, ref Fields ).Trim().Split( ' ' ) )[ 0 ].Trim();
			Category			= firstFields[ 2 ].Trim();
			ServID				= firstFields[ 0 ].Trim().Substring( 0, 3 );

			IsIgnorable			= false;

			string firstField = firstFields[ 0 ].Trim();

			// SeqNo				= Utilities.StaticNumberFormat( Convert.ToInt32( firstField.Substring( 5, firstField.Length - 5 ) ), 4 );
			if (firstField.Length == 5) 
			{
				SeqNo				= Utilities.StaticNumberFormat( Convert.ToInt32( firstField.Substring( 3, firstField.Length - 3 ) ), 4 );
			} 
			else if (firstField.Length == 4) 
			{
				SeqNo				= Utilities.StaticNumberFormat( Convert.ToInt32( firstField.Substring( 3, firstField.Length - 3 ) ), 4 );
			}
			else 
			{
				SeqNo				= Utilities.StaticNumberFormat( Convert.ToInt32( firstField.Substring( 5, firstField.Length - 5 ) ), 4 );
			}
//			Date				= FormatDate( lastFields[ 4 ].Trim(), lastFields[ 3 ].Trim(), lastFields[ 1 ].Trim().Substring( 0, 2) );
//			Time				= FormatTime( lastFields[ 1 ].Trim().Substring( 2, 4 ), lastFields[ 2 ].Trim() );//GetFieldValue( 0, 0, "", true, true, ref Fields );
			Date				= Utilities.GetCurrentDate();
			Time				= Utilities.GetCurrentTime();

			BureauSignature		= Source.ToUpper();

			
//			StoryField			= ReplaceIfPreviousIsSame( ' ', ConcatFieldsRange( start, end, "</p><p>\r\n" /* class=brdtekst>\r\n" */ ), "&nbsp;" );
//			StoryField			= ReplaceIfPreviousIsSame( ' ', ConcatFieldsRange( start, end, "<BR>\r\n" /* class=brdtekst>\r\n" */ ), "&nbsp;" );
			StoryField			= ConcatFieldsRange( start, end, "\r\n" ).Replace( "\t", "" );
			StoryField			= HttpUtility.HtmlEncode( StoryField );
			StoryField			= ReplaceIfPreviousIsSame( ' ', StoryField, "&nbsp;", '\n' ).Replace( "\r\n&nbsp;", "<BR>\r\n&nbsp;" );

			StoryFieldDoc		= ConcatFieldsRange( start, end, "\r\n" );
		}
	}
}
