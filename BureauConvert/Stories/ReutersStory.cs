using System;
using System.Web;

namespace ConDis
{
	/// <summary>
	/// Klasse for byrået Reuters
	/// </summary>
	public class ReutersStory : ConDis.Story
	{
		public ReutersStory()
		{
			//
		}

		public ReutersStory( string filename, bool analyze, bool isTest, ref Configuration conf )
		{
			NotabeneSubdirectory = "Reuters";
			SportsCategory = "SPO";
			EconomyCategory = "OEC";
			EditorialCategory = "DED";
			EditorialCategory2 = "OSM";

			IsTest = isTest;
			FullOriginalPath = filename;
			EditedText = ReadFile( FullOriginalPath, "", true, false );

			if( analyze )
			{
				Analyze();
			}

			ExportFileName = GenerateFilename( GetNewSerialNumber( ) );
		}

		public override void SetIsPrintable()
		{
			IsPrintable = false;

			if( ( Convert.ToInt32( Priority ) < 3 ) && ( ! IsSports ) )
			{
				IsPrintable = true;
			}
			else if( KeyWords.ToLower().IndexOf( "urgent" ) > -1 )
			{
				IsPrintable = true;
			}
		}

		public override void GetFieldValues()
		{
			Priority			= GetFieldValue( 4, 1, "(", false, false, ref Fields );
			KeyWords			= GetFieldValue( 11, 1, "(", false, false, ref Fields );
			KeyWords			= RemoveCharIfNonTextual( KeyWords, 0, '\0', false );
//			Source				= GetFieldValue( 16, 2, "(", false, false, ref Fields );
			Source				= " Reuters";
			Category			= GetFieldValue( 1, 2, "", false, false, ref Fields );
			ServID				= "REU";

			IsIgnorable			= false;

			SeqNo				= ( GetFieldValue( 0, 1, "(", false, false, ref Fields ) ).Substring( 4, 4 );
//			Date				= ( ( GetFieldValue( 6, 1, "(", false, false, ref Fields ) ).Insert( 4, kDateSeparator ) ).Insert( 7, kDateSeparator );
//			Time				= FormatTime( GetFieldValue( 8, 1, "(", false, false, ref Fields ), "" );
			Date				= Utilities.GetCurrentDate();
			Time				= Utilities.GetCurrentTime();

			
			string headerSymbol = RemoveCharIfNonTextual( GetFieldValue( 5, 0, "", true, true, ref Fields ), 0, '\0', false ).Trim();

			if( headerSymbol == "i" )
			{
				Header			= RemoveCharIfNonTextual( GetFieldValue( 4, 0, "", true, true, ref Fields ), 0, '\0', false );
			}
			else
			{
				Header			= "( > Ingen overskrift angitt < )";
			}

			BureauSignature		= Source.ToUpper();

			StoryFieldDoc		= GetFieldValue( 0, 0, "", true, true, ref Fields ).Remove( 0, 1 );
//			StoryField			= ReplaceIfPreviousIsSame( ' ', StoryFieldDoc, "&nbsp;" ).Replace( "<BR>", "</p><p>" ); // class=brdtekst>" );
//			StoryField			= ReplaceIfPreviousIsSame( ' ', StoryFieldDoc, "&nbsp;" ).Replace( "\r\n\r\n", "<_CRLF_>" ).Replace( "\r\n", "" ).Replace( "<_CRLF_>", "\r\n" ).Replace( "<BR>", "<BR>\r\n" );
//			StoryField			= HttpUtility.HtmlEncode( StoryField.Replace( "\t", "" ) );

			StoryField			= ReplaceIfPreviousIsSame( ' ', StoryFieldDoc, "&nbsp;", '\n' );
			StoryField			= StoryField.Replace( "\r\n&nbsp;", "<BR>\r\n&nbsp;" );
			StoryField			= StoryField.Replace( "\r\n", "<BR>\r\n" );

//			StoryField			= StoryFieldDoc.Replace( "\r\n\r\n", "<_CRLF_>" ).Replace( "\r\n", "" ).Replace( "<_CRLF_>", "\r\n" );
			StoryFieldDoc		= StoryFieldDoc.Replace( "&nbsp;", " " ).Replace( "<BR>", "" );
		}
	}
}
