using System;
using System.Web;
using System.Globalization;
using System.Text.RegularExpressions;

namespace ConDis
{
	/// <summary>
	/// Summary description for APStory.
	/// </summary>
	public class APStory : ConDis.Story
	{
		public APStory()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public APStory( string filename, bool analyze, bool isTest, ref Configuration conf )
		{
			NotabeneSubdirectory = "AP";
			SportsCategory = "SPO";
			EconomyCategory = "f";
			EditorialCategory = "k";
			EditorialCategory2 = "v";

			IsTest = isTest;
			FullOriginalPath = filename;
			EditedText = ReadFile( FullOriginalPath, "", false, false );

			//Extra celaning bad NTB tags
			EditedText = EditedText.Replace("#lt;","<");
			EditedText = EditedText.Replace("#gt;",">");
			EditedText = System.Web.HttpUtility.HtmlDecode(EditedText);
			EditedText = EditedText.Replace("<<","<");
			EditedText = EditedText.Replace(">>",">");
			
			//Remove DTD spec as the DTD ruins opening the XML with an XML parser
			EditedText = EditedText.Replace("<!DOCTYPE nitf SYSTEM \"nitf.dtd\">","<!--DOCTYPE nitf SYSTEM \"nitf.dtd\"-->");
			EditedText = EditedText.Replace("<!DOCTYPE NITF SYSTEM \"nitf.dtd\">","<!--DOCTYPE NITF SYSTEM \"nitf.dtd\"-->");

			//Some general cleanup
			EditedText = EditedText.Replace("<TEXT>","<TEXT>\n"); 
			EditedText = EditedText.Replace("</TEXT>","\n</TEXT>"); 

			Regex rx = new Regex("&(?!(apos|quot|#|amp))");
			EditedText = rx.Replace(EditedText,"&amp;");
			
			string APEuroSymbol = Convert.ToChar(0xC2).ToString() + Convert.ToChar(0x80).ToString();
			string APEuroSymbolReplacement = "EUR ";
			EditedText = EditedText.Replace(APEuroSymbol, APEuroSymbolReplacement);
			
			//rx = new Regex("<(?!/)");
			//EditedText = rx.Replace(EditedText,"\n<");

			if ( CountStrings(EditedText,"\n") <= 3 )
			{
				EditedText = EditedText.Replace("</"," \n</"); 
				EditedText = EditedText.Replace("<","\n<"); 
			}
			
			#region "Disse to vaskene er tvilsomme hacks. De �delegger for tabellene fra TT og kan brekke i fremtiden"

			//FNB headers mess up
			EditedText = EditedText.Replace("<Header>",""); 
			EditedText = EditedText.Replace("</Header>",""); 
			EditedText = EditedText.Replace("<Company>",""); 
			EditedText = EditedText.Replace("</Company>",""); 

			//And Swedish foppal tables
			EditedText = EditedText.Replace("<SPLINJE LVAD=\"Annan\">",""); 

			#endregion

				 
			//Clean up the "XML" a little
			bool inHead = false;
			bool inText = false;
			int po=0, pc=0, otpos=0;

			String delim = "\r\n";
			string[] lines = EditedText.Split(delim.ToCharArray());
			for ( int i = 0; i < lines.GetLength(0); i++ )
			{
				//Skip blank lines
				if ( lines[i] == "" ) continue;

					//TRack our position in the XML an do some work at certain positions
				else if ( lines[i].StartsWith("<HEAD>") ) inHead = true;                
				else if ( lines[i].StartsWith("</HEAD>") ) inHead = false;                
				else if ( lines[i].StartsWith("<TEXT>") ) 
				{ 
					inText = true; 
					otpos = i; 
				}
				else if ( lines[i].StartsWith("</TEXT>") ) 
				{
					if ( po < pc )
					{
						for (int j = 0; j < (pc - po); j++ )
							lines[otpos] += "<P>";
					}
					else if ( po > pc )
					{
						for (int j = 0; j < (po - pc); j++ )
							lines[i] = "</P>" + lines[i];
					}
					inText = false;
				}
				else if ( lines[i].StartsWith("<ORD>") ) 
				{
					lines[i] = lines[i].Replace("<P>","</P><P>");					
					lines[i] = lines[i].Replace("<ORD>","<ORD><P>");					
					lines[i] += "</P>";
				}
				
				//Clean up the header with close tags/quotes
				if ( inHead && (CountQuotes(lines[i]) % 2) > 0 ) lines[i] = lines[i].Replace(">","\">");
				if ( inHead && lines[i].EndsWith("\">") ) lines[i] = lines[i].Replace("\">","\" />");
			
				//Count number of opening/closing P's
				if ( inText ) 
				{
					po += CountStrings(lines[i],"<P>");	
					pc += CountStrings(lines[i],"</P>");	
				}
			}

			EditedText = "";
			for ( int i = 0; i < lines.GetLength(0); i++ )
			{
				if ( lines[i] == "" ) continue;
				EditedText += lines[i] + "\r\n";
			}

			if( analyze )
			{
				Analyze();
			}

			ExportFileName = GenerateFilename( GetNewSerialNumber() );
		}

		public int CountQuotes(string s)
		{
			int c = 0;
			for (int i = 0; i < s.Length ; i++)
			{
				if ( s.ToCharArray()[i].Equals('"') ) c++;
			}

			return c;
		}

		public int CountStrings(string s, string find)
		{
			int c = 0, pos = 0;
			while ( (pos = s.IndexOf(find,pos)) > -1 )
			{
				pos++;
				c++;
			}

			return c;
		}

		public string LookupSourceName(string s)
		{
			switch(s)
			{
				case "NTB":
					return "NTB";
				case "FNB":
					return "FNB";
				case "TTN":
					return "TT";
				case "RBN":
					return "RB";
				default:
					return s;
			}
		}

		public override void SetIsPrintable()
		{
			IsPrintable = false;
			try
			{
				if( (Convert.ToInt32( Priority ) < 4 ) && ( ! IsSports ) )
				{
					IsPrintable = true;
				}
			}
			catch 
			{
				IsPrintable = false;
			}

		}

		public override void GetFieldValues()
		{
			//string[] firstFields = GetFieldValue( 1, 0, "", false, false, ref Fields ).Trim().Split( ' ' );
			//string[] lastFields = GetFieldValue( 0, 0, "", true, true, ref Fields ).Trim().Split( ' ' );

			//Load the XML
			System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
			try
			{
				doc.LoadXml(EditedText);
			} 
			catch (Exception ex)
			{
				throw ex;
			}


			try
			{
				Priority			= doc.SelectSingleNode("/nitf/head/docdata/urgency/@ed-urg").Value;
				if(Priority == "3")
				{
					Priority = "2";
				}
			}
			catch
			{
				Priority			= "5";
			}

			
			KeyWords			= doc.SelectSingleNode("/nitf/head/docdata/du-key/@key").InnerText.Trim();
			Source				= LookupSourceName(doc.SelectSingleNode("/nitf/head/docdata/doc-id/@regsrc").InnerText.Trim());
			ServID				= LookupSourceName(doc.SelectSingleNode("/nitf/head/docdata/doc-id/@regsrc").InnerText.Trim());
			IsIgnorable = (KeyWords.ToUpper().IndexOf("LINIECHECK") >= 0);

			//Stoffgruppe
			Category			= doc.SelectSingleNode("/nitf/head/meta[@name='ap-category']/@content").InnerText.Trim();
			if ( Category == "s") Category= "SPO";
						
			//ServID				= doc.SelectSingleNode("/NORNITF/HEAD/SERVID").InnerText.Trim();

			//SeqNo				= doc.SelectSingleNode("/NORNITF/HEAD/NR").InnerText.Trim();
			
			Date				= doc.SelectSingleNode("/nitf/head/docdata/date.issue/@norm").InnerText.Trim();
			//Time				= doc.SelectSingleNode("/NORNITF/HEAD/TIMESENT").InnerText.Trim();

			DateTime dt = DateTime.Now;
			if ( Date != "")
				dt = DateTime.ParseExact(Date,"yyyyMMddTHHmmssZ",CultureInfo.InvariantCulture);
				
			Date				= dt.ToString("yyyy-MM-dd");
			Time				= dt.ToString("HH.mm");

			try
			{
				Header				= doc.SelectSingleNode("/nitf/body/body.head/hedline").InnerText.Trim();
			} 
			catch
			{
				Header = " > Ingen overskrift < ";
			}

			
			//Lead and to-red
			if ( doc.SelectSingleNode("/nitf/body/body.head/byline") != null )
			{
				System.Xml.XmlNodeList nodes = doc.SelectNodes("/nitf/body/body.head/byline");
				if ( nodes.Count > 1 )
				{
					StoryField = "<P><i>" + nodes[0].InnerText.Trim() + "</i></P>";
					StoryField += "<P><B>" + nodes[1].InnerText.Trim() + "</B></P>";
				}
				else 
					StoryField = "<P><i>" + doc.SelectSingleNode("/nitf/body/body.head/byline").InnerText.Trim() + "</i></P>";

				StoryFieldDoc = doc.SelectSingleNode("/nitf/body/body.head/byline").InnerText.Trim() + "\r\n";
			}

			//if ( doc.SelectSingleNode("/nitf/body/body.head/abstract") != null)
			//{
			//	StoryField += "<P><B>" + doc.SelectSingleNode("/nitf/body/body.head/abstract").InnerText.Trim() + "</B></P>";
			//	StoryFieldDoc += doc.SelectSingleNode("/nitf/body/body.head/abstract").InnerText.Trim();
			//}
			if ( doc.SelectSingleNode("/nitf/body/body.head/dateline") != null )
			{
				StoryField += "<P>";
				System.Xml.XmlNodeList nodes = doc.SelectNodes("/nitf/body/body.head/dateline");
				if( nodes.Count > 1 )
				{
					foreach(System.Xml.XmlNode node in nodes)
					{
						StoryField += node.InnerText.Trim();
						StoryFieldDoc += node.InnerText.Trim();
					}
				}
				else
				{
					StoryField += doc.SelectSingleNode("/nitf/body/body.head/dateline").InnerText.Trim();
					StoryFieldDoc += doc.SelectSingleNode("/nitf/body/body.head/dateline").InnerText.Trim();
				}
				
				StoryField += "</P>";
				StoryFieldDoc += "\r\n";
			}

			if ( doc.SelectSingleNode("/nitf/body/body.content/block") != null ) 
			{
				System.Xml.XmlNodeList nodes = doc.SelectSingleNode("/nitf/body/body.content/block").ChildNodes;
				foreach(System.Xml.XmlNode node in nodes)
				{
					if(node.Name == "p")
					{
						StoryField			+= "<P>" + node.InnerXml.Trim() + "</P>"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerXml.Trim();
						StoryFieldDoc		+= node.InnerText.Trim() + "\r\n"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerText.Trim();
					}
					if(node.Name == "table")
					{
						StoryField			+= "<table>" + node.InnerXml.Trim() + "</table>"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerXml.Trim();
						StoryFieldDoc		+= node.InnerText.Trim() + "\r\n"; //doc.SelectSingleNode("/nitf/body/body.content/block").InnerText.Trim();
					}
				}
			}
			else
			{
				try
				{
					StoryField			+= doc.SelectSingleNode("/nitf/body/body.content").InnerXml.Trim();
					StoryFieldDoc		+= doc.SelectSingleNode("/nitf/body/body.content").InnerText.Trim();
				} 
				catch
				{
					StoryField			+= doc.SelectSingleNode("/nitf/body").InnerXml.Trim();
					StoryFieldDoc		+= doc.SelectSingleNode("/nitf/body").InnerText.Trim();
				}
			}
		}	
	}
}
