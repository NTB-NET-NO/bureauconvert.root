using System;
using System.Web;

namespace ConDis
{
	/// <summary>
	/// Klasse for byr�et AFP
	/// </summary>
	public class DPATyskStory : ConDis.Story
	{
		public DPATyskStory()
		{
			//
		}

		public DPATyskStory( string filename, bool analyze, bool isTest, ref Configuration conf )
		{
			NotabeneSubdirectory = "DPA tysk";
			SportsCategory = "sp";
			EconomyCategory = "w*";
			EditorialCategory = "rs";
			EditorialCategory2 = "rs";

			IsTest = isTest;
			FullOriginalPath = filename;
			EditedText = ReadFile( FullOriginalPath, "", true, true );

			if( analyze )
			{
				Analyze();
			}

            ExportFileName = GenerateFilename(GetNewSerialNumber());
		}

		public override void SetIsPrintable()
		{
			IsPrintable = false;
			string printToken = "(Eil";

			if( ( Convert.ToInt32( Priority ) < 3 ) && ( ! IsSports ) )
			{
				IsPrintable = true;
			}
			else if( Header.Length >= printToken.Length )
			{
				if( Header.ToLower().Substring( 0, printToken.Length ) == printToken.ToLower() )
				{
					IsPrintable = true;
				}
			}
		}

		public override void GetFieldValues()
		{
			// string[] firstFields = GetFieldValue( 3, 0, "", false, false, ref Fields ).Trim().Split( ' ' );
			// string[] lastFields = GetFieldValue( 0, 0, "", true, true, ref Fields ).Trim().Split( ' ' );
			string[] firstFields = GetFieldValue( 1, 0, "", false, false, ref Fields ).Trim().Split( ' ' );
			string[] lastFields = GetFieldValue( 0, 0, "", true, true, ref Fields ).Trim().Split( ' ' );

			Priority			= firstFields[ 1 ].Trim();
			// KeyWords			= FormatDPAKeyWords( GetFieldValue( 5, 0, "", false, false, ref Fields ).Trim() );
			KeyWords			= FormatDPAKeyWords( GetFieldValue( 2, 0, "", false, false, ref Fields ).Trim() );
			Source				= " DPAtysk"; //lastFields[ 0 ].Trim(); // ( GetFieldValue( 0, 0, "", true, true, ref Fields ).Trim().Split( ' ' ) )[ 0 ].Trim();
			BureauSignature		= Source.ToUpper();
			Category			= firstFields[ 2 ].Trim();
			ServID				= firstFields[ 0 ].Trim().Substring( 0, 3 );

			IsIgnorable			= ( Category.ToLower() == "ck" );

			string firstField = firstFields[ 0 ].Trim();

			SeqNo				= Utilities.StaticNumberFormat( Convert.ToInt32( firstField.Substring( 3, firstField.Length - 3 ) ), 3 );
//			Date				= FormatDate( lastFields[ 3 ].Trim(), lastFields[ 2 ].Trim(), lastFields[ 0 ].Trim().Substring( 0, 2) );
//			Time				= FormatTime( lastFields[ 0 ].Trim().Substring( 2, 4 ), lastFields[ 1 ].Trim() );//GetFieldValue( 0, 0, "", true, true, ref Fields );
			Date				= Utilities.GetCurrentDate();
			Time				= Utilities.GetCurrentTime();
			// Header				= GetFieldValue( 6, 0, "", false, false, ref Fields );
			Header				= GetFieldValue( 4, 0, "", false, false, ref Fields );

			if( Header.Length == 0 )
			{
				Header = "[No header found]";
			}

			Header				= RemoveCharIfNonTextual( Header, Header.Length - 1, ')' ).Trim();
			BureauSignature		= Source.ToUpper();

			GetFieldValue( 5, 0, "", false, false, ref Fields );
			int start = LastFieldGotten;
			GetFieldValue( 1, 0, "", true, true, ref Fields );
			int end = LastFieldGotten;
			
//			StoryField			= "&nbsp;&nbsp;&nbsp;" +
//				ReplaceIfPreviousIsSame( ' ', ConcatFieldsRange( start, end, "<BR>\r\n" ), "&nbsp;" ).Replace( "\r\n\r\n", "<_CRLF_>" ).Replace( "\r\n", "" ).Replace( "<_CRLF_>", "<BR>\r\n&nbsp;&nbsp;&nbsp;" );
//			StoryField			= HttpUtility.HtmlEncode( StoryField );
			StoryField			= ConcatFieldsRange( start, end, "\r\n" ).Replace( "\t", "" );
			StoryField			= HttpUtility.HtmlEncode( StoryField );
			StoryField			= ReplaceIfPreviousIsSame( ' ', StoryField, "&nbsp;", '\n' ).Replace( "\r\n&nbsp;", "<BR>\r\n&nbsp;&nbsp;&nbsp;" ).Replace( "\r\n ", "<BR>\r\n&nbsp;&nbsp;&nbsp;" );
			StoryField			= StoryField.Replace( "\r\n", "<BR>\r\n" );

			StoryFieldDoc		= ConcatFieldsRange( start, end, "\r\n" );
		}
	}
}
