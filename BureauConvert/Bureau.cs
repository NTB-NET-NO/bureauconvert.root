using System;
using System.IO;
using System.Configuration;

namespace ConDis
{
	/// <summary>
	/// 
	/// </summary>
	public class Bureau
	{
		public string IncomingStoryDirectory = "";
		public string ArchivedStoryDirectory = "";
		public string HTMLStoryDirectory = "";
		public string DocStoryDirectory = "";
		public string PrintStoryDirectory = "";
//		public string FullFiffusWirePath = "";
		public string FailedDirectory = "";
		public string Name = "";
		
		public Bureau( string name )
		{
			this.Name = name;
			SetPaths();
		}

		private void SetPaths()
		{
            //this.IncomingStoryDirectory = CompletePath( Config.Get( "IncomingPath" ), true );
            //this.ArchivedStoryDirectory = CompletePath( Config.Get( "ArchivePath" ), true );
            //this.HTMLStoryDirectory = CompletePath(		Config.Get( "HTMLPath" ), false );
            //this.DocStoryDirectory = CompletePath(		Config.Get( "DocPath" ), false );
            //this.PrintStoryDirectory = CompletePath(	Config.Get( "PrintPath" ), false );
            //this.FullFiffusWirePath = CompletePath(		Config.Get( "FullFiffusWirePath" ), false );
            //this.FailedDirectory = CompletePath(		Config.Get( "FailedDirectory" ), false );
            //this.IncomingStoryDirectory = CompletePath(BureauConvert.Properties.Settings.Default.IncomingPath, true);
            //this.ArchivedStoryDirectory = CompletePath(BureauConvert.Properties.Settings.Default.ArchivePath, true);
            //this.HTMLStoryDirectory = CompletePath(BureauConvert.Properties.Settings.Default.HTMLPath, false);
            //this.DocStoryDirectory = CompletePath(BureauConvert.Properties.Settings.Default.DocPath, false);
            //this.PrintStoryDirectory = CompletePath(BureauConvert.Properties.Settings.Default.PrintPath, false);
            // this.FullFiffusWirePath = CompletePath(BureauConvert.Properties.Settings.Default.FullFiffusWirePath,false);
            //this.FailedDirectory = CompletePath(BureauConvert.Properties.Settings.Default.FailedDirectory, false);

            this.IncomingStoryDirectory = CompletePath(BureauConvert.Properties.Settings.Default.IncomingPath, true);
            this.ArchivedStoryDirectory = CompletePath(BureauConvert.Properties.Settings.Default.ArchivePath, true);
            this.HTMLStoryDirectory = CompletePath(BureauConvert.Properties.Settings.Default.HTMLPath, false);
            this.DocStoryDirectory = CompletePath(BureauConvert.Properties.Settings.Default.DocPath, false);
            this.PrintStoryDirectory = CompletePath(BureauConvert.Properties.Settings.Default.PrintPath, false);
            this.FailedDirectory = CompletePath(BureauConvert.Properties.Settings.Default.FailedDirectory, false);
		}

		private string CompletePath( string parentDir, bool useSubirectories )
		{
			string tmp = parentDir;
			

			if( useSubirectories )
			{
				tmp += "\\" + this.Name;
				
			}
			
			return tmp;
		}
	}
}
