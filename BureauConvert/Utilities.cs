using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace ConDis
{
	/// <summary>
	/// Summary description for Utilities.
	/// </summary>
	public class Utilities
	{		
		public Utilities()
		{
			Initialize();
		}
		public static void Initialize()
		{
		}

		public static string WildcardToRegex( string pattern )
		{
			return "^" + Regex.Escape( pattern ).Replace("\\*", ".*").Replace("\\?", ".") + "$";
		}

		public static string CharEntityEncode( string str )
		{
			return CharEntityEncode( str, false );
		}

		public static string CharEntityEncode( string str, bool encodeSpaces )
		{
			foreach( HTMLEntity h in Program.HTMLEntities )
			{
				if( encodeSpaces )
				{
					str = str.Replace( h.Character, h.CharacterEntity );
				}
				else if( h.Character != " " )
				{
					str = str.Replace( h.Character, h.CharacterEntity );
				}
			}

			return str;
		}

//		public static string ReplaceCharacters (string str) 
//		{
//			NITFcharacters = new ArrayList();
//
//			NITFcharacters.Add( new NITFcharacters( "&thinsp;",	" " ) );	// "mykt mellomrom
//
//			NITFcharacters.Add( new NITFcharacters( "&ndash;",	"&#150;", 	"�" ) );	// sitatstrek
//			
//
//			str = str.Replace (str, "");
//			return str;
//		}

		public static string InsertAfterFirstOccurrence( string str, string token, string insertStr )
		{
			int i = 0;
			return InsertAfterFirstOccurrence( str, token, insertStr, ref i );
		}

		public static string InsertAfterFirstOccurrence( string str, string token, string insertStr, ref int indexAfterInsertedString )
		{
			indexAfterInsertedString = -1;

			if( str.Length > 0 && token.Length > 0 )
			{
				int idx = str.IndexOf( token );

				if( idx > -1 )
				{
					str = str.Insert( idx + token.Length, insertStr );

					if( idx + token.Length + insertStr.Length <= str.Length )
					{
						indexAfterInsertedString = idx + token.Length + insertStr.Length;
					}
				}
			}
			return str;
		}

		public static string InsertAfterFirstOccurrence( string str, string token, string insertStr, int offset )
		{
			string substring = str.Substring( offset, str.Length - offset );

			int i = 0;
			return str.Substring( 0, offset ) + InsertAfterFirstOccurrence( substring, token, insertStr, ref i );
		}


		public static string FormatTimeElement( int e )
		{
			string str = e.ToString();

			if( str.Length < 2 )
			{
				str = "0" + str;
			}

			return str;
		}

		public static string StaticNumberFormat( int e, int length )
		{
			string str = e.ToString();

			while( str.Length < length )
			{
				str = "0" + str;
			}

			return str;
		}

		public static string GetCurrentTime()
		{
			return DateTime.Now.ToString( "HH.mm" );
		}

		public static string GetCurrentDate()
		{
			return DateTime.Now.Date.ToString( "yyyy-MM-dd" );
		}
	}
}
