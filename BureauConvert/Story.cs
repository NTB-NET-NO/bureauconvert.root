using System;
using System.Collections;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;
using System.Xml;
using System.Text;
using System.Globalization;
using System.Web;
using BureauConvert;

namespace ConDis
{
	/// <summary>
	/// Baseklasse for de ulike byr�ene
	/// </summary>
	public class Story
	{
		public ArrayList Fields = new ArrayList();

		private bool TagInserted = false;
		public bool IsTest = false;

		public string Priority = "";
		public string KeyWords = "";
		public string Source = "";
		public string Category = "";
		public string ServID = "";
		public string SeqNo = "";
		public string Date = "";
		public string Time = "";
		public string Header = "";
		public string Body = "";
		public string StoryField = "";
		public string StoryFieldDoc = "";
		public string HtmlSource = "";
		public string DocSource = "";
		public string FiffusNewLineSource = "";
		public string BureauSignature = "";

		public string SportsCategory = "";
		public string EditorialCategory = "";
		public string EditorialCategory2 = "";
		public string EconomyCategory = "";
		//public string SportsCategory = "";

		public string FIPHeader = "";
		public string HTMLHeader = "";
		public string DocHeader = "";
		public string Author = "";

		public string NotabeneSubdirectory = "";

		public string IncomingFolder = "";

		//Bureau Bureau;

		public string QT;
//		public string PR;
//		public string PS;
//		public string GR;
//		public string SU;
//		public string SS;

		public bool IsSports	= false;
		public bool IsEconomy	= false;
		public bool IsOtherNews	= false;
		public bool IsEditorial	= false;

		public bool IsURGENT	= false;
		public bool IsIgnorable	= false;
		public bool IsPrintable	= false;

		private const string kDelimiter = "<DLIM>";
		//		private const string kParagraphStart = "<p>";
		//		private const string kParagraphEnding = "</p>";
		private const string kParagraphEnding = "<BR>\r\n&nbsp;";

		public string PrevChar = "";
		public string PrevString = "";
		public string EditedText = "";
		public string FullOriginalPath = "";
		public string ExportFileName = "";
		public string kDateSeparator = "-";

		private const string HTML_HeaderFont = "";
		private const string HTML_SemiHeaderFont = "";
		private const string HTML_BodyFont = "";
		private const string HTML_AuthorFont = "";
		private const string HTML_BureauSignatureFont = "";

		public const string Style = "<style>\r\n"
			+ "p.Infolinje, li.Infolinje, div.Infolinje\r\n"
			+ "{mso-style-name:_Infolinje;}\r\n"
			+ "p.Brdtekst, li.Brdtekst, div.Brdtekst\r\n"
			+ "{mso-style-name:_Br\\00F8dtekst;}\r\n"
			+ "p.Tabellkode, li.Tabellkode, div.Tabellkode\r\n"
			+ "{mso-style-name:_Tabellkode;}\r\n"
			+ "</style>\r\n";

		// Get the local time zone and a base local time.
		static TimeZone localZone = TimeZone.CurrentTimeZone;

		public int LastFieldGotten = -1;


		public Bureau CurrentBureau;


		public Story()
		{
			Initialize();
		}

//		public Story( string filename )
//		{
//			FileName = filename;
//			Initialize();
//		}

		private void Initialize()
		{
//			QT = KeyWords;
//			PR = Priority;
//			PS = Priority.ToString();
//			GR = Category;
//			SU = Source;
//			SS = Header;

			QT = "";
//			PR = Priority;
//			PS = Priority.ToString();
//			GR = Category;
//			SU = Source;
//			SS = KeyWords;

			Fields = new ArrayList();
		}

		public char Chr(int i)
		{
			//Return the character of the given character value
			return Convert.ToChar(i);
		}

		public void Analyze()
		{
			GetFields( ref Fields, EditedText, false );
			GetFieldValues();
			DetrmineCategory();
			SetIsPrintable();
			GenerateDocumentParts();

			string newsType = "";

			if( IsSports )
			{
				newsType = "sport";
			}
			else
			{
				newsType = "nyheter";
			}

			HtmlSource = HTMLHeader + Body;
			DocSource = FIPHeader + Header + "\r\n   \r\n" + StoryFieldDoc;

			// Creating the line that will be put in the fiffus index-file
			// When we are working with full feeds
			if ((IncomingFolder == "ttfull") |
				(IncomingFolder == "rbfull") |
				(IncomingFolder == "fnbfull"))
			{
				Source = IncomingFolder;
			}
			FiffusNewLineSource = Source.Replace( " ", "" ).Replace( "#", "<Hash>" ) + "#" +
				DateTime.Now.Date.ToString( "dd.MM.yyyy" ) + "#" +
				DateTime.Now.ToString( "HH.mm" ) + "#" +
				newsType.Replace( "#", "<Hash>" ) + "#" +
				Priority.Replace( "#", "<Hash>" ) + "#" +
				Utilities.CharEntityEncode( KeyWords ).Replace( "#", "" ) + "#";
//				HttpUtility.HtmlEncode( KeyWords.Replace( "#", "<Hash>" ) ) + "#";
//				Header.Replace( "#", "<Hash>" ) + "#";
				//+ "/wire/data/20060406/TT/nyheter/06se0154.htm\r\n";
		}

        /// <summary>
        /// This method creates the print source
        /// </summary>
        /// <returns></returns>
		public string GeneratePrintSource()
		{
			string print = "";
			string story = "";
			string indent = "  ";
			string line = indent;

			int charsPerLine = 70;

			string[] sentences = StoryFieldDoc.Split( '\r' );

			foreach( string s in sentences )
			{
				string[] words = s.Replace( "\n", "" ).Split( ' ' );

				foreach( string word in words )
				{
					string w = word.Trim();

					// We need to do some HTML translating here I believe

					if( line == "" )
					{
						line += w;
					}
					else if( ( line + " " + w ).Length < charsPerLine )
					{
						line += " " + w;
					}
					else
					{
						story += line + "\r\n";
						line = indent + w;
					}
				}

				story += line + "\r\n";
				line = indent;
			}
			
			print =  "**************************************************************************\r\n";
			print += "Kilde: \t\t"	+ Source	+ "\r\n";
			print += "Tid: \t\t"	+ Time		+ "\r\n";
			print += "Stikkord: \t"	+ KeyWords	+ "\r\n";
			print += "Prioritet:\t"	+ Priority	+ "\r\n";
			print += "**************************************************************************\r\n\r\n";
			print += story + "\r\n";
			print += "<......Slutt......>";

			// Try and convert the print here from 1252 encoding to 8859
//			Encoder enc = new Encoder();
//			enc.
			return print;
		}


/*
		public void ResetArray(IPrintEngine pe,
			Graphics g,
			RectangleF clipRect)
		{
			arrayLines.Clear();
			float pageWidth= clipRect.Width- (this.horizontalMargin*2);
			char newLineChar= '\r';
			char space= ' ';
			// use tabAsString later to convert tabs to spaces
			StringBuilder tabBuilder= new StringBuilder();
			for(int i=0; i<this.tabSize; i++)
			{
				tabBuilder.Append(space);
			}
			string tabAsString= tabBuilder.ToString();

			// get properties and parse text
			Brush brush;
			Font font;
			StringFormat stringFormat;
			if (this.isUseDocumentProperties)
			{
				brush= pe.Brush;
				font= pe.Font;
				stringFormat= pe.StringFormat;
			}
			else
			{
				brush= new SolidBrush(Color.Black);
				font= this.Font;
				stringFormat= this.stringFormat;
			}
			string text= this.Text;
			if (this.isReplaceTokens)
			{
				text= pe.ReplaceTokens(text);
			}

			// break out lines with embedded line break
			string temp= this.Text.Replace(System.Environment.NewLine,"\r");
			string[] arrayUnfittedLines= temp.Split(newLineChar);

			// break out lines that overflow
			foreach (string line in arrayUnfittedLines)
			{
				// replace tabs with spaces
				string parsed;
				parsed= line.Replace("\t",tabAsString);
				// add to array if line of word tokens fits in page margin
				if (g.MeasureString(parsed,font,
					Int32.MaxValue,
					stringFormat).Width <= pageWidth)
				{
					arrayLines.Add(parsed);
				}
				else
					// line does not fit on page
					// need to break up line into word tokens
					// and then place tokens into lines that fit in page margin
				{
					string[] arrayTokens= parsed.Split(space); // tokenize line
					StringBuilder buffer;
					int index=0;
					while (index < arrayTokens.Length)
					{
						buffer= new StringBuilder();
						// at least one token on line, ? may be clipped
						buffer.Append(arrayTokens[index]+space);
						index++;
						while(index< arrayTokens.Length &&
							g.MeasureString(buffer.ToString()+arrayTokens[index],
							font,
							Int32.MaxValue,
							stringFormat).Width <= pageWidth)
						{
							buffer.Append(arrayTokens[index]+space);
							index++;
						}
						// end of tokens or end of line
						arrayLines.Add(buffer.ToString());
					}
				}
			}
			this.currentIndex= 0;
		}

*/

//		public string Format( string str )
//		{
//
//		}
		public string ReplaceIfPreviousIsSame( char sameChar, string source, string replacement )
		{
			return ReplaceIfPreviousIsSame( sameChar, source, replacement, '\0' );
		}

		public string ReplaceIfPreviousIsSame( char sameChar, string source, string replacement, char intro )
		{
			char current = '\r', previous;
			bool onARoll = false;
			bool considerIntro = intro != '\0';
			int rollStart = -1, rollCount = 0;

			string srcCopy = source;

			for( int i = 0; i < srcCopy.Length; i++ )
			{
				previous = current;
				current = srcCopy[ i ];

				if( previous == current && sameChar == current)
				{
					if( considerIntro && i > 1 )
					{
						if( srcCopy[ i - 2 ] == intro )
						{
							if( ! onARoll )
							{
								onARoll = true;
								rollStart = i - 1;
								rollCount = 2;
							}
						}
					}
					else if( ! onARoll )
					{
						onARoll = true;
						rollStart = i - 1;
						rollCount = 2;
					}
					else
					{
						rollCount++;
					}
				}				
				else
				{
					if( onARoll )
					{
						srcCopy = srcCopy.Remove( rollStart, rollCount );

						for( int j = 0; j < rollCount; j++ )
						{
							srcCopy = srcCopy.Insert( rollStart, replacement );
						}

						i = rollStart + ( rollCount * replacement.Length );
					}

					onARoll = false;
					rollCount = 0;
				}
			}

			return srcCopy;
		}

		public void GetFields( ref ArrayList fields, string tmpStr, bool useLineNumbers )
		{
			int idx = -1;
			string lnStr = "";

			while( tmpStr.IndexOf( kDelimiter ) > -1 )
			{
				idx = tmpStr.IndexOf( kDelimiter );

				if( useLineNumbers )
				{
					lnStr = fields.Count.ToString() + ": ";
				}

				fields.Add( lnStr + ( tmpStr.Substring( 0, idx ) ).TrimEnd() );
				tmpStr = tmpStr.Remove( 0, idx + kDelimiter.Length );
			}

			if( fields.Count > 0 )
			{
				while( fields[ fields.Count - 1 ].ToString() == "" )
				{
					fields.RemoveAt( fields.Count - 1 );

					if( fields.Count == 0 )
					{
						break;
					}
				}
			}

			fields.Add( "" );
		}

		public virtual void GetFieldValues()
		{
		}

		public virtual void SetIsPrintable()
		{
		}

		public void GenerateDocumentParts()
		{
//			Body = 
//				"<BODY>\r\n" + 
//				"<span style='font-size:15.0pt;font-family:\"Arial\"'><p>" + // class=Infolinje>" + 
//				Header + "</p></span>\r\n" +
//				"<span style='font-size:11.0pt;font-family:\"Times New Roman\"'><p>" + // class=brdtekst>\r\n" + 
//				StoryField + 
//				"\r\n</p>\r\n</span></BODY>\r\n";

			if( Source == "AFP" )
			{
//				Body = 
//					"<BODY>\r\n" + 
//					"<span style='font-size:15.0pt;font-family:\"Arial\"'><p>" + // class=Infolinje>" + 
//					HttpUtility.HtmlEncode( Header ) + "</p></span>\r\n" +
//					"<span style='font-size:12.0pt;font-family:\"Times New Roman\"'><b>" + // class=brdtekst>\r\n" + 
//					Utilities.InsertAfterFirstOccurrence( StoryField, "\r\n", "</b>" ) + 
//					"\r\n</span></BODY>\r\n";

				int j = 0;
				string token = "\r\n<BR>\r\n";

				if( StoryField.IndexOf( "<BR>" ) == 0 )
				{
					token = "<BR>\r\n";
				}

				StoryField = Utilities.InsertAfterFirstOccurrence( StoryField, token, "<b>", ref j );

				if( j > -1 )
				{
					StoryField = Utilities.InsertAfterFirstOccurrence( StoryField, "<BR>", "</b>", j );
				}

				Body = 
					"<BODY>\r\n" + 
					"<span style='font-size:15.0pt;font-family:\"Arial\"'><p>" + // class=Infolinje>" + 
					HttpUtility.HtmlEncode( Header ) + "</p></span>\r\n" +
					"<span style='font-size:12.0pt;font-family:\"Times New Roman\"'>" + // class=brdtekst>\r\n" + 
					StoryField + 
					"\r\n</span></BODY>\r\n";
			}
			else
			{
				Body = 
					"<BODY>\r\n" + 
					"<span style='font-size:15.0pt;font-family:\"Arial\"'><p>" + // class=Infolinje>" + 
					HttpUtility.HtmlEncode( Header ) + "</p></span>\r\n" +
					"<span style='font-size:12.0pt;font-family:\"Times New Roman\"'>" + // class=brdtekst>\r\n" + 
					StoryField + 
					"\r\n</span></BODY>\r\n";
			}

			HTMLHeader = "<HTML>\r\n<HEAD>\r\n";
			HTMLHeader +="<TITLE>" + KeyWords + "</TITLE\r\n>";
			HTMLHeader +="\r\n<META name='prioritet' content='" + Priority + "'>";
			HTMLHeader +="\r\n<META name='stikkord' content='" + HttpUtility.HtmlEncode( KeyWords ) + "'>";
			HTMLHeader +="\r\n<META name='kilde' content='" + HttpUtility.HtmlEncode( Source ) + "'>"; 
			HTMLHeader +="\r\n<META name='kategori' content='" + HttpUtility.HtmlEncode( Category ) + "'>";
			HTMLHeader +="\r\n<META name='servid' content='" + HttpUtility.HtmlEncode( this.ServID ) + "'>";
			HTMLHeader +="\r\n<META name='seqno' content='" + SeqNo + "'>";
			HTMLHeader +="\r\n<META name='dato' content='" + Date + "'>";
			HTMLHeader +="\r\n<META name='tid' content='" + Time + "'>";

			HTMLHeader += Style;

			HTMLHeader +="\r\n</HEAD>\r\n<BODY bgcolor='FFFFFF'>\r\n<TABLE bgcolor='#ffeedd' cellspacing='5'>";
			HTMLHeader +="\r\n<TR>\n<TD colspan='3'><B>Stikkord: </B>"+ HttpUtility.HtmlEncode( KeyWords ) + "</TD>";
			HTMLHeader +="\r\n<TD><B>Kilde: </B>" + HttpUtility.HtmlEncode( Source ) + "</TD><TR>";
			HTMLHeader +="\r\n<TD><B>Dato: </B>" + Date + "</TD>";
			HTMLHeader +="\r\n<TD><B>Tid: </B>" + Time + "</TD>";
			HTMLHeader +="\r\n<TD><B>Prioritet: </B>" + Priority + "</TD>";
			HTMLHeader +="\r\n<TD><B>Kategori: </B>" + HttpUtility.HtmlEncode( Category ) + "</TD>\r\n</TR>\r\n</TABLE>\r\n";

			
            
            /**
             * Here we are building the Exchange-paths. 
             * It is of great importance that the paths does not end with /
             * paths are separated with : (colon)
             * @rev: Trond Hus�, 11.05.2010
             */

            
            string notabenePath = "";
            
			if( IsTest )
			{
				KeyWords = "Test";
				notabenePath = "NOTABENE\\Byr�er\\NTB";
			}
			else
			{
				notabenePath = "NOTABENE\\Byr�er\\Alle byr�er:NOTABENE\\Byr�er\\" + NotabeneSubdirectory;

//				if( Category.ToUpper() != SportsCategory.ToUpper() )
//				{
				if( ! IsSports )
				{
                    //if ((IncomingFolder.ToLower() != "fnbfull") |
                    //    (IncomingFolder.ToLower() != "ttfull") |
                    //    (IncomingFolder.ToLower() != "rbfull")) 
                    //{
                    //    // notabenePath += NotabeneSubdirectory;
                    //    // I shouldn't do anything here really
                    //}
                    notabenePath += ":NOTABENE\\Byr�er\\alle nyheter (ikke sport)";
				}
//				else
//				{
//					IsSports = true;
//				}

				// bureau = new Bureau();

                /**
                 * Nordisk linje Norden-k�en
                 */
                if ((Source.ToUpper() == "TT")  & (IncomingFolder.ToLower() == "") |
					(Source.ToUpper() == "RB") & (IncomingFolder.ToLower() == "") |
					(Source.ToUpper() == "FNB") & (IncomingFolder.ToLower() == "")) 
				{
                    notabenePath = "NOTABENE\\Byr�er\\Norden";
				}

                /**
                 * Fullfeed'en fra TT, Ritzau og FNB skal vises i Alle byr�er-k�en 
                 * og i sine "egne" respektive mapper: 
                 * FNB (full feed)
                 * Ritzau (full feed) 
                 * TT (full feed)
                 * Og i alle nyheter (ikke sport) - men ikke sportsmeldingene...
                 */
                if ((IncomingFolder.ToLower() == "fnbfull") |
                    (IncomingFolder.ToLower() == "ttfull") |
                    (IncomingFolder.ToLower() == "rbfull"))
                {
                    if (! IsSports)
                    {
                        notabenePath = "NOTABENE\\Byr�er\\Alle byr�er:NOTABENE\\Byr�er\\Alle nyheter (ikke sport):NOTABENE\\Byr�er\\" + NotabeneSubdirectory;
                    }
                    else
                    {
                        notabenePath = "NOTABENE\\Byr�er\\Alle byr�er:NOTABENE\\Byr�er\\" + NotabeneSubdirectory;
                    }
                }

				if (Source.ToUpper() == "REUTERSFINANS") 
				{
					notabenePath = "NOTABENE\\Byr�er\\" + NotabeneSubdirectory;
				}
			}

			
			//QT = "";

//			PR = Priority;
//			PS = Priority.ToString();
//			GR = Category;
//			SU = Source;
//			SS = KeyWords;


//RE=NTBBeskjedTilRed
//DI=NTBDistribusjonsKode
//KH=NTBHovedKategorier
//ID=NTBID
//KA=NTBKanal
//SU=NTBKilde
//SI=NTBMeldingsSign
//QT=NTBMeldingsType 	(Nyheter, Sport, etc.)
//MV=NTBMeldingsVersjon
//PR=NTBPrioritet
//PS=NTBPrioritetString
//SS=NTBStikkord 		(Stikkord fra header)
//GR=NTBStoffgruppe 		(Dette er Kategori i Wire)
//UG=NTBUndergruppe
//KS=NTBUnderKatSport
//OM=NTBOmraader
//LF=NTBFylker

			FIPHeader = "~";
			FIPHeader += "\r\nGR:" + Category;
			FIPHeader += "\r\nQT:" + QT;
			FIPHeader += "\r\nQF:text";
			FIPHeader += "\r\nQD:" + notabenePath;
			FIPHeader += "\r\nSU:" + Source;
			FIPHeader += "\r\nSS:" + KeyWords;
			FIPHeader += "\r\nST:" + KeyWords;
			FIPHeader += "\r\nPR:" + Priority;
			FIPHeader += "\r\nPS:" + Priority;
			FIPHeader += "\r\n~\r\n";

			Author = "Terje";

			DocHeader = 
//				"<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" \r\n" + 
//				"xmlns:w=\"urn:schemas-microsoft-com:office:word\" \r\n" +
//				"xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" \r\n" +
//				"xmlns:st1=\"urn:schemas-microsoft-com:office:smarttags\" " +
//				"xmlns=\"http://www.w3.org/TR/REC-html40\">" +
				"\r\n<head>" +
//				"\r\n<META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-16\">" +
//				"\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">" +
//				"\r\n<meta name=\"ProgId\" content=\"Word.Document\">" +
//				"\r\n<meta name=\"Generator\" content=\"Microsoft Word 11\">" +
//				"\r\n<meta name=\"Originator\" content=\"Microsoft Word 11\">" +
//				"\r\n<o:SmartTagType namespaceuri=\"urn:schemas-microsoft-com:office:smarttags\" name=\"place\"/><xml><o:DocumentProperties>" +
//								
//				"\r\n<o:Author>" + Author + "</o:Author>" +
//				"\r\n<o:LastAuthor>" + Author + "</o:LastAuthor>" +
//				"\r\n<o:Company>NTB</o:Company>" +
//				"\r\n</o:DocumentProperties>" +
//				"\r\n<o:CustomDocumentProperties>" +
//				"\r\n<o:Subject dt:dt=\"String\">" + KeyWords + "</o:Subject>" +
//				"\r\n<o:NTBPrioritet dt:dt=\"float\">" + Priority + "</o:NTBPrioritet>" +
//				"\r\n<o:NTBPrioritsString dt:dt=\"string\">" + Priority + "</o:NTBPrioritsString>" +
//				"\r\n<o:NTBMeldingsType dt:dt=\"string\">" + KeyWords + "</o:NTBMeldingsType>" +
//				"\r\n<o:NTBMeldingsSign dt:dt=\"string\">dnt/</o:NTBMeldingsSign>" +
//				//				"\r\n<o:NTBTjeneste dt:dt=\"string\">Nyhetstjenesten</o:NTBTjeneste>" +
//				"\r\n<o:NTBTjeneste dt:dt=\"string\">" + Source + "</o:NTBTjeneste>" +
//				"\r\n<o:NTBStoffgruppe dt:dt=\"string\">" + Category + "</o:NTBStoffgruppe>" +
//				//"<o:NTBUndergruppe dt:dt=\"string\">Tabeller og resultater</o:NTBUndergruppe>" +
//				//"<o:NTBHovedkategorier dt:dt=\"string\">Sport</o:NTBHovedkategorier>" +
//				"\r\n<o:NTBStikkord dt:dt=\"string\">" + KeyWords + "" + "</o:NTBStikkord>" +
//				//"<o:NTBUnderKatSport dt:dt=\"string\">Trav</o:NTBUnderKatSport>" +
//				//"<o:NTBDistribusjon dt:dt=\"string\">Nyhetstjenesten (ALL)</o:NTBDistribusjon>" +
//				//"<o:NTBKanal dt:dt=\"string\">A</o:NTBKanal>" +
//				"\r\n<o:NTBKilde dt:dt=\"string\">" + Source + "" + "</o:NTBKilde>" +
//				//"\r\n<o:QD dt:dt=\"string\">NOTABENE\\Byr�er\\" + Source + "</o:QD>" +
//				"\r\n<o:foldername dt:dt=\"string\">" + notabenePath + "</o:foldername>" +
//				//				"\r\n<o:message-type dt:>IPM.Note.Byr_melding</o:message-type>" +
//				"\r\n</o:CustomDocumentProperties></xml>\r\n" +
				Style +
				"\r\n</head>";
		}

		public string GetNewSerialNumber()
		{
            int sn = BureauConvert.Properties.Cfg.Default.SerialNumber;
			sn++;
            
            BureauConvert.Properties.Cfg.Default.SerialNumber = sn;
			return sn.ToString();
		}

		public string RemoveCharIfNonTextual( string source, int index )
		{
			return RemoveCharIfNonTextual( source, index, '\0' );
		}

		public void DetrmineCategory()
		{
			IsSports = CategoryIsMatch( Category, SportsCategory );
			
			if( ! IsSports )
			{
				IsEconomy = CategoryIsMatch( Category, EconomyCategory );
				
				if( ! IsEconomy )
				{
					IsEditorial = ( CategoryIsMatch( Category, EditorialCategory ) || CategoryIsMatch( Category, EditorialCategory2 ) );

					if( ! IsEditorial )
					{
						IsOtherNews = true;
						QT = "Nyheter";
					}
					else
					{
						QT = "Tilred";
					}
				}
				else
				{
					QT = "�konomi";
				}
			}
			else
			{
				QT = "Sport";
			}
		}

		public bool CategoryIsMatch( string category, string matchPattern )
		{
			return Regex.IsMatch( category.ToUpper(), Utilities.WildcardToRegex( matchPattern.ToUpper() ) );
		}

		public string FormatDPAKeyWords( string keyWords )
		{
			if( keyWords.Length > 0 )
			{
				if( keyWords[ 0 ] == '/' )
				{
					keyWords = keyWords.Remove( 0, 1 );
				}

				keyWords = keyWords.Replace( "/", "-" );
			}

			return keyWords;
		}

		public string RemoveCharIfNonTextual( string source, int index, char exception )
		{
			return RemoveCharIfNonTextual( source, index, exception, true );
		}

		public string RemoveCharIfNonTextual( string source, int index, char exception, bool acceptNumbers )
		{
			if( source.Length > 0 )
			{
				char c = source[ index ];

				if( !( exception == c ) )
				{
					if( ! acceptNumbers )
					{
						if( ! ( ( c >= 65 && c <= 90 ) || ( c >= 97 && c <= 122 ) || ( c >= 128 && c <= 151 ) || ( c >= 153 && c <= 157 ) || ( c >= 159 && c <= 168 ) ) )
						{
							source = source.Remove( index, 1  );
						}
					}
					else if( ! ( ( c >= 48 && c <= 57 ) || ( c >= 65 && c <= 90 ) || ( c >= 97 && c <= 122 ) || ( c >= 128 && c <= 151 ) || ( c >= 153 && c <= 157 ) || ( c >= 159 && c <= 168 ) ) )
					{
						source = source.Remove( index, 1  );
					}
				}
			}

			return source;
		}



		public string FormatTime( string str, string timeStandard )
		{
			int localZone = 1;
			timeStandard = timeStandard.ToUpper();

			int h = Convert.ToInt32( str.Substring( 0, 2 ) );
			int m = Convert.ToInt32( str.Substring( 2, 2 ) );
			int zone = 0;
				
			if( timeStandard == "" )
			{
				zone = Convert.ToInt32( str.Substring( 7, 2 ) );
			}
			else if ( timeStandard == "UTC" )
			{
				if( IsDaylightSummertime( DateTime.Now ) )
				{
					localZone += 1;
				}
			}

			h = ( h + zone + localZone ) % 24;

			return Utilities.FormatTimeElement( h ) + "." + Utilities.FormatTimeElement( m );
		}

		public bool IsDaylightSummertime( DateTime dt )
		{
			DaylightTime daylight = localZone.GetDaylightChanges( dt.Year );
			return ( dt > daylight.Start && dt < daylight.End );
		} 

		public string FormatDate( string year, string month, string day )
		{
			int iDay = Convert.ToInt32( day );
			int iMonth = 1;

			year = "20" + year;
			day = Utilities.FormatTimeElement( iDay );

			switch( month.Substring( 0, 3 ).ToUpper() )
			{
//				German months:
//
//				Jan Feb Mrz 
//				Apr Mai Jun 
//				Jul Aug Sep 
//				Okt Nov Dez 

//				French months:
//
//				JAN, FEV, MAR, AVR, MAI, JUI, JUL, AOU, SEP, OCT, NOV, DEC

				case "JAN":
					iMonth = 1;
					break;

				case "FEB":
				case "FEV":
					iMonth = 2;
					break;

				case "MAR":
				case "MRZ":
					iMonth = 3;
					break;

				case "APR":
				case "AVR":
					iMonth = 4;
					break;

				case "MAY":
				case "MAI":
					iMonth = 5;
					break;

				case "JUN":
				case "JUI":
					iMonth = 6;
					break;

				case "JUL":
					iMonth = 7;
					break;

				case "AUG":
				case "AOU":
					iMonth = 8;
					break;

				case "SEP":
					iMonth = 9;
					break;

				case "OCT":
				case "OKT":
					iMonth = 10;
					break;

				case "NOV":
					iMonth = 11;
					break;

				case "DEC":
				case "DEZ":
					iMonth = 12;
					break;
			}

			month = Utilities.FormatTimeElement( iMonth );
			return year + kDateSeparator + month + kDateSeparator + day;
		}

		public string GenerateFilename( string sn )
		{
			return Source.Replace( " ", "" ).ToUpper() + "_" + Date + "_" + Utilities.StaticNumberFormat( Convert.ToInt32( sn ), 8 );
		}

		public string GetFieldValue( int fieldIndex, int groupIndex, string groupToken, bool negativeIndex, bool useLastOccurence, ref ArrayList fields )
		{
			int offset = 0, factor = 1, index = 0;
			string retVal = "";

			if( negativeIndex )
			{
				factor = -1;
				fieldIndex += 1;
			}

			try
			{
				if( useLastOccurence )
				{
					offset = fields.LastIndexOf( groupToken );
				}
				else
				{
					for( int i = 0; i < groupIndex; i++ )
					{
						offset = fields.IndexOf( groupToken, offset, fields.Count - offset );

						if( offset == -1 ) 
						{
							offset = 0;
						}
						else
						{
							offset += 1;
						}					
					}
				}

				if( offset == -1 ) 
				{
					offset = 0;
				}

				LastFieldGotten = index = Math.Abs( ( offset + ( fieldIndex * factor ) ) );
				retVal = fields[ index ].ToString();
			}

			catch( Exception e )
			{
				throw e;
			}

			return retVal;
		}

		public string ConcatFieldsRange( int startIdx, int endIdx, string endOfString )
		{
			string tmpStr = "";

			int rangeLength = endIdx - startIdx + 1;
			
			for( int i = startIdx; i < startIdx + rangeLength; i++ )
			{
				tmpStr += Fields[ i ] + endOfString;
			}

			return tmpStr;
		}

		public string ReadFile(string FullPath, string ErrInfo, bool replaceChars, bool delimitLines )
		{
			string teksten = "";

			try 
			{
				FileStream fs = new FileStream( FullPath, FileMode.Open, FileAccess.Read );
				byte[] bytes = new byte[ fs.Length ];
				fs.Read( bytes, 0, Convert.ToInt32( fs.Length ) );
				//fs.Close();

				if( replaceChars )
				{
					int i;

					for (i = 0; i <= fs.Length - 1; i++) 
					{
						string bokstav;
						bokstav = Chr(bytes[i]).ToString();

						if( ( bytes[i] >= 0 & bytes[i] <= 8 ) | ( bytes[i] >= 11 & bytes[i] <= 12 ) | ( bytes[i] >= 14 & bytes[i] <= 31 ) ) 
						{
							if ( ! TagInserted )
							{
								bokstav = kDelimiter;
								TagInserted = true;
							}
							else
							{
								bokstav = "";
							}
						}
						else 
						{
							if( ( PrevChar == "\n" ) && ( bytes[i] != ' ' ) )
							{
								bokstav = " " + Chr( bytes[i] );
							}
							else if ( bytes[i] == 10 )
							{
								if( delimitLines )
								{
									bokstav = "";
								}
								else
								{
									// bokstav = bokstav;
								}
							}
							else if ( bytes[i] == 13 )
							{
								if( delimitLines )
								{
									bokstav = kDelimiter;
								}
								else
								{
									// bokstav = bokstav;//"<BR>" + bokstav;
								}
							}
							else if ( bytes[i] == 28 )
							{
								if( delimitLines )
								{
									bokstav = "";
								}
								else
								{
									bokstav = "";
								}
							}
							else if ( bytes[i] == 32 )
							{
								if( PrevChar == "\n" )
								{
									//									bokstav = kParagraphEnding;
									//									bokstav = kParagraphEnding;
								}
								else if (PrevString == "&nbsp;" || PrevString == kParagraphEnding )
								{
									//bokstav = "&nbsp;";
								}
							}
					
							TagInserted = false;
						}

						PrevString = bokstav;

						if ( bokstav.Length > 0 )
						{
							PrevChar = bokstav.Substring( bokstav.Length - 1, 1 );
						}
						else
						{
							PrevChar = "";
						}
					
						teksten += bokstav;
					}

				}
			
				if( teksten != "" && teksten.Substring( 0, kDelimiter.Length ) != kDelimiter )
				{
					teksten = teksten.Insert( 0, kDelimiter );
				}

				if (teksten == "")
				{
					teksten = new string(Encoding.GetEncoding("iso-8859-1").GetChars(bytes));
				}

				fs.Close();
			} 
			catch( Exception e ) 
			{
				throw e;
			}

			return teksten;	
		}

	}
}
