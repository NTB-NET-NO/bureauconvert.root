using System;
using System.IO;
//using System.Web;
//using System.Web.Mail;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.Configuration;
//using AxSHDocVw;
using mshtml;
using ConDis;


namespace BureauConvert
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		/* TO DO:
		 * 
		 * OK - Legge options fra formen inn i konfigurasjonsfil
		 * OK - Sette opp arkivering av kildefiler
		 * OK - Sette opp sletting av kildefiler
		 * OK - Utvidet sikring av kode med Try
		 * OK - Skrive normale hendelser til logg
		 * OK - Skrive feil til logg
		 * OK - Lage options for hva som skal kj�res
		 * OK - Sende mail ved feil
		 * OK - Legge inn kode for oppdatering av siste 12 timer for fiffus wire
		 * OK - Skille ut kjernelogikk fra MainForm til egen klasse
		 * OK - Legge inn koden i en service
		 * OK - Lage installasjonsprosjekt for servicen
		 * 
		 * OK - Lage options for hva programmet skal gj�re - html og/eller doc
		 * OK - Lage printfunksjon
		 * 
		 * OK - Direkte print mot valgt printer fra BereauConvert
		 * OK - Legge om hvor HTML-filer lagres (katalogstruktur)
		 * OK - Mapping av byr�spesifikk kode for kategori mot nyheter/sport
		 *		Reuters - "SPO"
		 *		AFP - "S"
		 *		DPA Engelsk - "s"
		 *		DPA Tysk - "sp"
		 * 
		 * OK - Legge til ekstra destinasjon i Notabene ut ifra kategori
		 * 
		 * OK - Hvorfor lagres Cfg.xml i hytt og pine?
		 * OK - Sett opp font size p� utskrift
		 * OK - Flytt filer som har feilet til folderen "Failed"
		 * 
		 * OK - Noen Reutersmeldinger f�r Source = "en"
		 * OK - AFP nyhet f�r annen kategori, "CZ" kontra "C"
		 * OK - Endre ingress til Bold
		 * 
		 * Samle archive-logikk
		*/
//		Bekrefter med dette oppgaven med � fjerne linjeskift for � skape en mer flytende tekst og forbeholde 
//		linjeskift til avsnitt. Jeg tar dette n�r eventuell feilretting og lignende er unnagjort og testingen 
//		begynner � komme i m�l.

//		Torgeir �nsket for �vrig at mer kosmetiske ting tas med meg direkte, siden han i slike sammenhenger kun blir et mellomledd.

//		Notater under debugging:

//		Flytting av filer som feiler, er kommentert ut.
//		Print er kommentert ut.

		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.TextBox textBox2;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public static Program gProgram;

		public System.Windows.Forms.Label lblPri;
		public System.Windows.Forms.Label label1;
		public System.Windows.Forms.Label label2;
		public System.Windows.Forms.Label lblKW;
		public System.Windows.Forms.Label label3;
		public System.Windows.Forms.Label label4;
		public System.Windows.Forms.Label label5;
		public System.Windows.Forms.Label label6;
		public System.Windows.Forms.Label label7;
		public System.Windows.Forms.Label label8;
		public System.Windows.Forms.Label label9;
		public System.Windows.Forms.Label lblSrc;
		public System.Windows.Forms.Label lblCat;
		public System.Windows.Forms.Label lblTime;
		public System.Windows.Forms.Label lblDate;
		public System.Windows.Forms.Label lblSeqNo;
		public System.Windows.Forms.Label lblSrvId;
		public System.Windows.Forms.Label lblHeader;
		public System.Windows.Forms.Label label12;
		public System.Windows.Forms.Label lblBSignature;
		public System.Windows.Forms.TabControl tabControl1;
		public System.Windows.Forms.TabPage tabPage1;
		public System.Windows.Forms.TabPage tabPage2;
		public System.Windows.Forms.TabPage tabPage3;
		public System.Windows.Forms.RichTextBox richTextBox4;
		public System.Windows.Forms.RichTextBox richTextBox5;
		public System.Windows.Forms.TabControl tabControl2;
		public System.Windows.Forms.TabPage tabPage4;
		public System.Windows.Forms.TabPage tabPage5;
		public System.Windows.Forms.TabPage tabPage6;
		public System.Windows.Forms.RichTextBox richTextBox1;
		public System.Windows.Forms.RichTextBox richTextBox2;
		public System.Windows.Forms.RichTextBox richTextBox3;
		public System.Windows.Forms.Panel panel1;
		// public AxSHDocVw.AxWebBrowser axWebBrowser;
		public System.Windows.Forms.Button saveHtmlBtn;
		public System.Windows.Forms.Button button3;
		public System.Windows.Forms.SaveFileDialog saveFileDialog1;
		public System.Timers.Timer IntervalTimer;
		public System.Windows.Forms.CheckBox cbAnalyze;
		public System.Windows.Forms.ComboBox cbBureaus;
		public System.Windows.Forms.CheckBox cbTest;
		public System.Windows.Forms.Label lblCounter;
		public System.Windows.Forms.CheckBox cbShow;
		public System.Timers.Timer ErrorIntervalTimer;
		private System.Windows.Forms.Button button2;
        private WebBrowser wbViewer;
		public System.Windows.Forms.Button btnStart;

		public MainForm()
		{
			//
			// Required for Windows Form Designer support
			//
			gProgram = new Program( this );

			InitializeComponent();

			cbAnalyze.Checked = gProgram.DoAnalyze;
			cbTest.Checked = gProgram.DoTest;
			cbShow.Checked = gProgram.DoShowInGUI;

			object flags = 0;
			object targetFrame = String.Empty;
			object postData = String.Empty;
			object headers = String.Empty;
            //axWebBrowser.Navigate( "about:blank",ref flags, ref
            //    targetFrame, ref postData, ref headers );
            wbViewer.Navigate("about:blank");

			cbBureaus.SelectedIndex = 0;

			ErrorIntervalTimer.Start();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.lblPri = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblKW = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblSrc = new System.Windows.Forms.Label();
            this.lblCat = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblSeqNo = new System.Windows.Forms.Label();
            this.lblSrvId = new System.Windows.Forms.Label();
            this.lblHeader = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblBSignature = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.saveHtmlBtn = new System.Windows.Forms.Button();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.richTextBox5 = new System.Windows.Forms.RichTextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.IntervalTimer = new System.Timers.Timer();
            this.btnStart = new System.Windows.Forms.Button();
            this.cbAnalyze = new System.Windows.Forms.CheckBox();
            this.cbBureaus = new System.Windows.Forms.ComboBox();
            this.cbTest = new System.Windows.Forms.CheckBox();
            this.lblCounter = new System.Windows.Forms.Label();
            this.cbShow = new System.Windows.Forms.CheckBox();
            this.ErrorIntervalTimer = new System.Timers.Timer();
            this.button2 = new System.Windows.Forms.Button();
            this.wbViewer = new System.Windows.Forms.WebBrowser();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IntervalTimer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorIntervalTimer)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(16, 24);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(752, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "D:\\Input";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(784, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Get file";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.Location = new System.Drawing.Point(16, 56);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(752, 20);
            this.textBox2.TabIndex = 4;
            this.textBox2.Text = "D:\\Input\\siste12.txt";
            // 
            // lblPri
            // 
            this.lblPri.Location = new System.Drawing.Point(72, 80);
            this.lblPri.Name = "lblPri";
            this.lblPri.Size = new System.Drawing.Size(240, 160);
            this.lblPri.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(16, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Priority:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "KeyWords:";
            // 
            // lblKW
            // 
            this.lblKW.Location = new System.Drawing.Point(72, 96);
            this.lblKW.Name = "lblKW";
            this.lblKW.Size = new System.Drawing.Size(240, 16);
            this.lblKW.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(16, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Source:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(16, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 16);
            this.label4.TabIndex = 11;
            this.label4.Text = "Category:";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(16, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "ServID:";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(16, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 16);
            this.label6.TabIndex = 13;
            this.label6.Text = "SeqNo:";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(16, 176);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 16);
            this.label7.TabIndex = 14;
            this.label7.Text = "Date:";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(16, 192);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 16);
            this.label8.TabIndex = 15;
            this.label8.Text = "Time:";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(16, 208);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 16);
            this.label9.TabIndex = 16;
            this.label9.Text = "Header:";
            // 
            // lblSrc
            // 
            this.lblSrc.Location = new System.Drawing.Point(72, 112);
            this.lblSrc.Name = "lblSrc";
            this.lblSrc.Size = new System.Drawing.Size(240, 16);
            this.lblSrc.TabIndex = 20;
            // 
            // lblCat
            // 
            this.lblCat.Location = new System.Drawing.Point(72, 128);
            this.lblCat.Name = "lblCat";
            this.lblCat.Size = new System.Drawing.Size(240, 16);
            this.lblCat.TabIndex = 21;
            // 
            // lblTime
            // 
            this.lblTime.Location = new System.Drawing.Point(72, 192);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(240, 16);
            this.lblTime.TabIndex = 25;
            // 
            // lblDate
            // 
            this.lblDate.Location = new System.Drawing.Point(72, 176);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(240, 16);
            this.lblDate.TabIndex = 24;
            // 
            // lblSeqNo
            // 
            this.lblSeqNo.Location = new System.Drawing.Point(72, 160);
            this.lblSeqNo.Name = "lblSeqNo";
            this.lblSeqNo.Size = new System.Drawing.Size(240, 16);
            this.lblSeqNo.TabIndex = 23;
            // 
            // lblSrvId
            // 
            this.lblSrvId.Location = new System.Drawing.Point(72, 144);
            this.lblSrvId.Name = "lblSrvId";
            this.lblSrvId.Size = new System.Drawing.Size(240, 16);
            this.lblSrvId.TabIndex = 22;
            // 
            // lblHeader
            // 
            this.lblHeader.Location = new System.Drawing.Point(72, 208);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(240, 32);
            this.lblHeader.TabIndex = 26;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(16, 240);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 16);
            this.label12.TabIndex = 19;
            this.label12.Text = "BS:";
            // 
            // lblBSignature
            // 
            this.lblBSignature.Location = new System.Drawing.Point(72, 240);
            this.lblBSignature.Name = "lblBSignature";
            this.lblBSignature.Size = new System.Drawing.Size(240, 16);
            this.lblBSignature.TabIndex = 29;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(16, 264);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(840, 392);
            this.tabControl1.TabIndex = 36;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(832, 366);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "HTML";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.wbViewer);
            this.panel1.Location = new System.Drawing.Point(8, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(816, 344);
            this.panel1.TabIndex = 33;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.saveHtmlBtn);
            this.tabPage2.Controls.Add(this.richTextBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(832, 366);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "HTML Source";
            // 
            // saveHtmlBtn
            // 
            this.saveHtmlBtn.Location = new System.Drawing.Point(736, 328);
            this.saveHtmlBtn.Name = "saveHtmlBtn";
            this.saveHtmlBtn.Size = new System.Drawing.Size(75, 23);
            this.saveHtmlBtn.TabIndex = 39;
            this.saveHtmlBtn.Text = "Save...";
            this.saveHtmlBtn.Click += new System.EventHandler(this.saveHtmlBtn_Click);
            // 
            // richTextBox4
            // 
            this.richTextBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox4.Location = new System.Drawing.Point(8, 8);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.Size = new System.Drawing.Size(816, 352);
            this.richTextBox4.TabIndex = 33;
            this.richTextBox4.Text = "";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Controls.Add(this.richTextBox5);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(832, 366);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Doc Source";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(624, 328);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 39;
            this.button3.Text = "Save...";
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // richTextBox5
            // 
            this.richTextBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox5.Location = new System.Drawing.Point(8, 8);
            this.richTextBox5.Name = "richTextBox5";
            this.richTextBox5.Size = new System.Drawing.Size(816, 352);
            this.richTextBox5.TabIndex = 36;
            this.richTextBox5.Text = "";
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Location = new System.Drawing.Point(432, 80);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(424, 184);
            this.tabControl2.TabIndex = 37;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.richTextBox1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(416, 158);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "Initial";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.richTextBox1.Location = new System.Drawing.Point(8, 8);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(400, 144);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.richTextBox2);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(416, 158);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "Fields[]";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox2.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.richTextBox2.Location = new System.Drawing.Point(8, 8);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(400, 144);
            this.richTextBox2.TabIndex = 6;
            this.richTextBox2.Text = "";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.richTextBox3);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(416, 158);
            this.tabPage6.TabIndex = 2;
            this.tabPage6.Text = "<BODY>";
            // 
            // richTextBox3
            // 
            this.richTextBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox3.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.richTextBox3.Location = new System.Drawing.Point(8, 8);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(400, 144);
            this.richTextBox3.TabIndex = 0;
            this.richTextBox3.Text = "";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "HTML files (*.htm)|*.htm";
            // 
            // IntervalTimer
            // 
            this.IntervalTimer.Interval = 15000;
            this.IntervalTimer.SynchronizingObject = this;
            this.IntervalTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.IntervalTimer_Elapsed);
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.ForeColor = System.Drawing.Color.Red;
            this.btnStart.Location = new System.Drawing.Point(344, 224);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(56, 23);
            this.btnStart.TabIndex = 38;
            this.btnStart.Text = "Start!";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // cbAnalyze
            // 
            this.cbAnalyze.Checked = true;
            this.cbAnalyze.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAnalyze.Location = new System.Drawing.Point(328, 192);
            this.cbAnalyze.Name = "cbAnalyze";
            this.cbAnalyze.Size = new System.Drawing.Size(104, 24);
            this.cbAnalyze.TabIndex = 39;
            this.cbAnalyze.Text = "Analyze";
            // 
            // cbBureaus
            // 
            this.cbBureaus.Items.AddRange(new object[] {
            "Norden",
            "Reuters",
            "AFP",
            "DPA-E",
            "DPA-T",
            "AP",
            "TTFullFeed",
            "RitzauFullFeed",
            "FNBFullFeed"});
            this.cbBureaus.Location = new System.Drawing.Point(320, 112);
            this.cbBureaus.Name = "cbBureaus";
            this.cbBureaus.Size = new System.Drawing.Size(104, 21);
            this.cbBureaus.TabIndex = 40;
            // 
            // cbTest
            // 
            this.cbTest.Location = new System.Drawing.Point(328, 168);
            this.cbTest.Name = "cbTest";
            this.cbTest.Size = new System.Drawing.Size(104, 24);
            this.cbTest.TabIndex = 41;
            this.cbTest.Text = "Test";
            // 
            // lblCounter
            // 
            this.lblCounter.Location = new System.Drawing.Point(336, 88);
            this.lblCounter.Name = "lblCounter";
            this.lblCounter.Size = new System.Drawing.Size(72, 16);
            this.lblCounter.TabIndex = 43;
            this.lblCounter.Text = "0";
            this.lblCounter.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbShow
            // 
            this.cbShow.Checked = true;
            this.cbShow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShow.Location = new System.Drawing.Point(328, 144);
            this.cbShow.Name = "cbShow";
            this.cbShow.Size = new System.Drawing.Size(104, 24);
            this.cbShow.TabIndex = 44;
            this.cbShow.Text = "Show in GUI";
            // 
            // ErrorIntervalTimer
            // 
            this.ErrorIntervalTimer.Enabled = true;
            this.ErrorIntervalTimer.Interval = 30000;
            this.ErrorIntervalTimer.SynchronizingObject = this;
            this.ErrorIntervalTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.ErrorIntervalTimer_Elapsed);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(784, 56);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 45;
            this.button2.Text = "Last 12";
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // wbViewer
            // 
            this.wbViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbViewer.Location = new System.Drawing.Point(0, 0);
            this.wbViewer.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbViewer.Name = "wbViewer";
            this.wbViewer.Size = new System.Drawing.Size(816, 344);
            this.wbViewer.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(872, 670);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.cbShow);
            this.Controls.Add(this.cbAnalyze);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblCounter);
            this.Controls.Add(this.cbTest);
            this.Controls.Add(this.cbBureaus);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblBSignature);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblSeqNo);
            this.Controls.Add(this.lblSrvId);
            this.Controls.Add(this.lblCat);
            this.Controls.Add(this.lblSrc);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblKW);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblPri);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "ErgoGroup BureauConverter";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.IntervalTimer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorIntervalTimer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void button1_Click(object sender, System.EventArgs e)
		{
			if( gProgram.LogFile == null )
			{
				gProgram.PrepareLogFile();
			}

			textBox1.Text = "";
            // Config.Get( "IncomingPath" );
            string directory = BureauConvert.Properties.Settings.Default.IncomingPath; 

			switch( cbBureaus.SelectedIndex )
			{
				case 0:
				{
					directory += "\\Reuters";
					break;
				}
				case 1:
				{
					directory += "\\AFP";
					break;
				}
				case 2:
				{
					directory += "\\Dpa eng";
					break;
				}
				case 3:
				{
					directory += "\\Dpa tysk";
					break;
				}
			}

			openFileDialog1.Reset();
			openFileDialog1.InitialDirectory = directory;

			if( openFileDialog1.ShowDialog() == DialogResult.OK )
			{
				Story s = new Story();
				Bureau bureau = new Bureau( "" );
				bool analyze = cbAnalyze.Checked;

				switch( cbBureaus.SelectedIndex )
				{
					// Reuters
					case 1:
					{
						ReutersStory rs = new ReutersStory( openFileDialog1.FileName, analyze, cbTest.Checked, ref gProgram.Conf );
						bureau = new Bureau( "Reuters" );
						s = rs;
						break;
					}
					// AFP
					case 2:
					{
						AFPStory rs = new AFPStory( openFileDialog1.FileName, analyze, cbTest.Checked, ref gProgram.Conf );
						bureau = new Bureau( "AFP" );
						s = rs;
						break;
					}
					// DPA Engelsk
					case 3:
					{
						DPAEngStory rs = new DPAEngStory( openFileDialog1.FileName, analyze, cbTest.Checked, ref gProgram.Conf );
						bureau = new Bureau( "DPA Engelsk" );
						s = rs;
						break;
					}				
					// DPA Tysk
					case 4:
					{
						DPATyskStory rs = new DPATyskStory( openFileDialog1.FileName, analyze, cbTest.Checked, ref gProgram.Conf );
						bureau = new Bureau( "DPA Tysk" );
						s = rs;
						break;
					}				
					// Norden
					case 0:
					{
						NordenStory rs = new NordenStory( openFileDialog1.FileName, analyze, cbTest.Checked, ref gProgram.Conf );
						bureau = new Bureau( "Norden" );
						s = rs;
						break;
					}				
					// AP
					case 5:
					{
						APStory rs = new APStory( openFileDialog1.FileName, analyze, cbTest.Checked, ref gProgram.Conf );
						bureau = new Bureau( "AP" );
						s = rs;
						break;
					}				
				}

				textBox1.Text = s.FullOriginalPath;
				richTextBox1.Text = s.EditedText;

				gProgram.FiffusNewLineSource = s.FiffusNewLineSource;
				gProgram.CurrentStory = s;

				if( analyze )
				{
					FillTBLines( ref s.Fields );

					lblPri.Text = s.Priority;
					lblKW.Text = s.KeyWords;
					lblSrc.Text = s.Source;
					lblCat.Text = s.Category;
					lblSrvId.Text = s.ServID;
					lblSeqNo.Text = s.SeqNo;
					lblDate.Text = s.Date;
					lblTime.Text = s.Time;
					lblHeader.Text = s.Header;
					lblBSignature.Text = s.BureauSignature;
					richTextBox3.Text = s.Body;

					// IHTMLDocument2 doc = (IHTMLDocument2)this.axWebBrowser.Document;
                    // IHTMLDocument2 doc = (IHTMLDocument2)wbViewer.Document;

                    System.Windows.Forms.HtmlDocument document =
                        this.wbViewer.Document;

                    document.Body.InnerHtml = richTextBox4.Text = gProgram.HtmlSource = s.HtmlSource;
					// doc.body.innerHTML = richTextBox4.Text = gProgram.HtmlSource = s.HtmlSource;
					richTextBox5.Text = gProgram.DocSource = s.DocSource;

					gProgram.ArchiveAndSaveDocs( bureau, s.ExportFileName, s.FullOriginalPath );
				}
			}
		}

		public void FillTBLines( ref ArrayList al)
		{
			richTextBox2.Lines = ( string[] )al.ToArray( typeof( string ) );
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			try
			{
				string PatternString = textBox2.Text;
				//string Text = richTextBox1.Text;
				Regex r = new Regex( PatternString );
				Match m = r.Match( "123abc456" );
				
				if( m.Success )
				{
					int test = m.Index;
					string Text;
					Text = test.ToString();
					MessageBox.Show("You must enter a name.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch
			{
				MessageBox.Show("Error");
			}
		}
		private void saveHtmlBtn_Click(object sender, System.EventArgs e)
		{
			saveFileDialog1.Filter = "Win HTML files (*.htm)|*.htm|Standard HTML files (*.html)|*.html";

			if( saveFileDialog1.ShowDialog() == DialogResult.OK && saveFileDialog1.FileName != "" )
			{
				gProgram.SaveHTML( saveFileDialog1.FileName );
			}
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			saveFileDialog1.Filter = "Word Document (*.doc)|*.doc";

			if( saveFileDialog1.ShowDialog() == DialogResult.OK && saveFileDialog1.FileName != "" )
			{
				gProgram.SaveDoc( saveFileDialog1.FileName );
			}
		}

		private void btnStart_Click(object sender, System.EventArgs e)
		{
//			CheckAllBureaus();
			// IntervalTimer.Interval = Convert.ToInt32( Config.Get( "PollingInterval" ) );
            IntervalTimer.Interval = Convert.ToInt32(BureauConvert.Properties.Settings.Default.PollingInterval); //(Config.Get("PollingInterval"));
			IntervalTimer.Enabled = true;
			IntervalTimer.Start();
		}

		private void IntervalTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			gProgram.CheckAllBureaus();		
		}


		public void UpdateFormBeforeAnalyze( ref Story s, int counter )
		{
			textBox1.Text = s.FullOriginalPath;
			richTextBox1.Text = s.EditedText;
			lblCounter.Text = counter.ToString();
		}

		public void UpdateFormDuringAnalyze( ref Story s )
		{
			if( cbShow.Checked )
			{
				ClearComponents();
				FillTBLines( ref s.Fields );
		
				lblPri.Text = s.Priority;
				lblKW.Text = s.KeyWords;
				lblSrc.Text = s.Source;
				lblCat.Text = s.Category;
				lblSrvId.Text = s.ServID;
				lblSeqNo.Text = s.SeqNo;
				lblDate.Text = s.Date;
				lblTime.Text = s.Time;
				lblHeader.Text = s.Header;
				lblBSignature.Text = s.BureauSignature;
				richTextBox3.Text = s.Body;
		
				// IHTMLDocument2 doc = (IHTMLDocument2)this.axWebBrowser.Document;
                System.Windows.Forms.HtmlDocument document =
                    this.wbViewer.Document;
                document.Body.InnerHtml = richTextBox4.Text = gProgram.HtmlSource = s.HtmlSource;
				// doc.body.innerHTML = richTextBox4.Text = gProgram.HtmlSource = s.HtmlSource;
				richTextBox5.Text = gProgram.DocSource = s.DocSource;
		
				this.Invalidate();
				Application.DoEvents();
			}
		}

		public void ClearComponents()
		{
			lblPri.Text = "";
			lblKW.Text = "";
			lblSrc.Text = "";
			lblCat.Text = "";
			lblSrvId.Text = "";
			lblSeqNo.Text = "";
			lblDate.Text = "";
			lblTime.Text = "";
			lblHeader.Text = "";
			lblBSignature.Text = "";
			richTextBox3.Text = "";

			richTextBox2.Clear();

            // this.axWebBrowser.Navigate( "about:blank" );
            wbViewer.Navigate("about:blank");

			
            
			richTextBox4.Text = "";
			richTextBox5.Text = "";
		}

		private void ErrorIntervalTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			gProgram.ErrorIntervalTimer_Elapsed( ErrorIntervalTimer.Interval );
		}

		private void button2_Click_1(object sender, System.EventArgs e)
		{
			gProgram.UpdateLast12( textBox2.Text );
		}
	}
}
