using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using ConDis;

namespace BCService
{
	public class BCService : System.ServiceProcess.ServiceBase
	{
		public System.Timers.Timer ErrorIntervalTimer;
		public System.Timers.Timer IntervalTimer;
		
		public static Program gProgram;
		public System.Timers.Timer Last12Timer;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public BCService()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitComponent call
		}

		// The main entry point for the process
		static void Main()
		{
			gProgram = new Program();
			System.ServiceProcess.ServiceBase[] ServicesToRun;
	
			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new BCService() };

			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ErrorIntervalTimer = new System.Timers.Timer();
			this.IntervalTimer = new System.Timers.Timer();
			this.Last12Timer = new System.Timers.Timer();
			((System.ComponentModel.ISupportInitialize)(this.ErrorIntervalTimer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.IntervalTimer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Last12Timer)).BeginInit();
			// 
			// ErrorIntervalTimer
			// 
			this.ErrorIntervalTimer.Enabled = true;
			this.ErrorIntervalTimer.Interval = 60000;
			this.ErrorIntervalTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.ErrorIntervalTimer_Elapsed);
			// 
			// IntervalTimer
			// 
			this.IntervalTimer.Enabled = true;
			this.IntervalTimer.Interval = 15000;
			this.IntervalTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.IntervalTimer_Elapsed);
			// 
			// Last12Timer
			// 
			this.Last12Timer.Enabled = true;
			this.Last12Timer.Interval = 301020;
			this.Last12Timer.Elapsed += new System.Timers.ElapsedEventHandler(this.Last12Timer_Elapsed);
			// 
			// BCService
			// 
			this.ServiceName = "Bureau Convert Service";
			((System.ComponentModel.ISupportInitialize)(this.ErrorIntervalTimer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.IntervalTimer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Last12Timer)).EndInit();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart( string[] args )
		{
			try 
			{
				ErrorIntervalTimer.Enabled = true;
				ErrorIntervalTimer.Start();
				// IntervalTimer.Interval = Convert.ToInt32( Config.Get( "PollingInterval" ) );
                IntervalTimer.Interval = Convert.ToInt32(BureauConvert.Properties.Settings.Default.PollingInterval);
				IntervalTimer.Enabled = true;
				IntervalTimer.Start();
				Last12Timer.Enabled = true;
				Last12Timer.Start();  
			} 
			catch (Exception e) 
			{
				Program p = new Program();

				p.WriteToLog(e.ToString(), 2);
			}
		}
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			try 
			{
				// TODO: Add code here to perform any tear-down necessary to stop your service.
			}
			catch (Exception e) 
			{
				Program p = new Program();

				p.WriteToLog(e.ToString(), 2);
			}
			
		}

		private void IntervalTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			gProgram.CheckAllBureaus();			
		}

		private void ErrorIntervalTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			gProgram.ErrorIntervalTimer_Elapsed( ErrorIntervalTimer.Interval );
		}

		private void Last12Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			gProgram.UpdateLast12( gProgram.GetLast12Filename() );
		}
	}
}
