using System;
using System.IO;
using System.Web;
using System.Net.Mail;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Printing;
using BureauConvert;
using System.Text.RegularExpressions;
using System.Threading;
using System.Management;
using System.Text;
using System.Globalization;
using System.Configuration;
using System.Diagnostics;

namespace ConDis
{
	/// <summary>
	/// Summary description for Program.
	/// </summary>
	public class Program
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>

		public string HtmlSource;
		public string DocSource;
		public string PrintSource;
		public string FiffusNewLineSource;

		public bool IsService = false;

		private double BusyTimeoutMSeconds = 180000.0;

		public ArrayList Bureaus;

		const int kRetryLimit = 15;
		const int kSleepMs = 10000;

		int FiffusWriteCounter = 0;

		public string csIncomingDirectory = "";
		public string csArchivedDirectory = "";
		public string csHTMLDirectory = "";
		public string csDocDirectory = "";

		public string FullLogPath = "";
		public string AccumulatedErrors = "";
		public string FiffusWirePath = "";
//		public string FullFiffusWirePath = "";

		public string ReutersName = "";
		public string ReutersFinansName = "";
		public string AFPName = "";
		public string DPAEngelskName = "";
		public string DPATyskName = "";
		public string NordenName = "";
		public string APName = "";
		public string TTFullName = "";
		public string RitzauFullName = "";
		public string FNBFullName = "";

		public string NordicFull = "";

        public int SerialNumber = 0;

		// 2010.03.15:
		// Newly added stories are TT Full feed, Ritzau Full feed and FNB Full feed
//		public string TTFullName = "";
//		public string RitzauFullName = "";
//		public string FNBFullName = "";

		public string PrinterName = "";
		public string CurrentFileName = "";

		public bool DoDoc = false;
		public bool DoHTML = false;
		public bool DoPrint = false;
		public bool DoLast12 = false;
		public bool DoAddToFiffus = false;
		public bool DoArchiveFiles = false;
		public bool DoDeleteFiles = false;
		public bool DoWarnViaEmail = false;

		public bool ShareWireFiles = false;
		public bool DoAnalyze = false;
		public bool DoTest = false;
		public bool DoShowInGUI = false;

		public bool PrintCurrent = false;
		public bool BusyConverting = false;

		public Configuration Conf;
		public StreamWriter LogFile;

		public Bureau CurrentBureau;
		public Story CurrentStory;

		public System.Timers.Timer BusyTimeoutTimer;

		private static MainForm pMainForm;

		public static ArrayList HTMLEntities;

		Font PrintFont = new Font( "Arial", 12 );
		StreamReader StreamToPrint;
		PrintDocument PrintDoc;
		ArrayList PendingFilesForDeletion;

		DateTime LastActionTime;

		public Program( MainForm mainForm )
		{
			pMainForm = mainForm;
			Initialize();
		}

		public Program()
		{
			Initialize();
			Utilities.Initialize();
			IsService = true;
		}

		private void Initialize()
		{
			GetConfigAttributes();
			// Conf = new Configuration();
			// Conf.Deserialize();
            SerialNumber = BureauConvert.Properties.Cfg.Default.SerialNumber;
			CurrentStory = new Story();
			PrintDoc = new PrintDocument();
            
			PendingFilesForDeletion = new ArrayList();

			LastActionTime = DateTime.Now;

            BusyTimeoutMSeconds = Convert.ToDouble(BureauConvert.Properties.Settings.Default.BusyTimeoutMSeconds);
            // BusyTimeoutMSeconds = Convert.ToDouble(configu
			// BusyTimeoutMSeconds = Convert.ToDouble( Config.Get( "BusyTimeoutMSeconds" ) );

//			this.BusyTimeoutTimer.Interval = Convert.ToInt32( Config.Get( "BusyTimeoutMSeconds" ) );
//			this.BusyTimeoutTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.BusyTimeoutTimer_Elapsed);

			HTMLEntities = new ArrayList();

			HTMLEntities.Add( new HTMLEntity( "&nbsp;",  	"&#160;",	" " ) );	// non-breaking space
			HTMLEntities.Add( new HTMLEntity( "&iexcl;", 	"&#161;", 	"�" ) );	// inverted exclamation mark
			HTMLEntities.Add( new HTMLEntity( "&cent;", 	"&#162;", 	"�" ) );	// cent sign
			HTMLEntities.Add( new HTMLEntity( "&pound;", 	"&#163;", 	"�" ) );	// pound sign
			HTMLEntities.Add( new HTMLEntity( "&curren;", 	"&#164;", 	"�" ) );	// currency sign
			HTMLEntities.Add( new HTMLEntity( "&yen;", 		"&#165;",	"�" ) );	// yen sign
			HTMLEntities.Add( new HTMLEntity( "&brvbar;", 	"&#166;", 	"�" ) );	// broken bar
			HTMLEntities.Add( new HTMLEntity( "&sect;", 	"&#167;",	"�" ) );	// section sign
			HTMLEntities.Add( new HTMLEntity( "&uml;", 		"&#168;",	"�" ) );	// diaeresis
			HTMLEntities.Add( new HTMLEntity( "&copy;", 	"&#169;", 	"�" ) );	// copyright sign
			HTMLEntities.Add( new HTMLEntity( "&ordf;", 	"&#170;", 	"�" ) );	// feminine ordinal indicator
			HTMLEntities.Add( new HTMLEntity( "&laquo;", 	"&#171;", 	"�" ) );	// left-pointing double angle quotation mark
			HTMLEntities.Add( new HTMLEntity( "&not;", 		"&#172;",	"�" ) );	// not sign
			// ( "&shy;", 	"&#173;",	" " )	// soft hyphen						This one is invisible. Left out.
			HTMLEntities.Add( new HTMLEntity( "&reg;", 		"&#174;",	"�" ) );	// registered sign
			HTMLEntities.Add( new HTMLEntity( "&macr;", 	"&#175;", 	"�" ) );	// macron
			HTMLEntities.Add( new HTMLEntity( "&deg;", 		"&#176;",	"�" ) );	// degree sign
			HTMLEntities.Add( new HTMLEntity( "&plusmn;", 	"&#177;", 	"�" ) );	// plus-minus sign
			HTMLEntities.Add( new HTMLEntity( "&sup2;", 	"&#178;", 	"�" ) );	// superscript two
			HTMLEntities.Add( new HTMLEntity( "&sup3;", 	"&#179;", 	"�" ) );	// superscript three
			HTMLEntities.Add( new HTMLEntity( "&acute;", 	"&#180;", 	"�" ) );	// acute accent
			HTMLEntities.Add( new HTMLEntity( "&micro;", 	"&#181;", 	"�" ) );	// micro sign
			HTMLEntities.Add( new HTMLEntity( "&para;", 	"&#182;", 	"�" ) );	// pilcrow sign
			HTMLEntities.Add( new HTMLEntity( "&middot;", 	"&#183;", 	"�" ) );	// middle dot
			HTMLEntities.Add( new HTMLEntity( "&cedil;", 	"&#184;", 	"�" ) );	// cedilla
			HTMLEntities.Add( new HTMLEntity( "&sup1;", 	"&#185;", 	"�" ) );	// superscript one
			HTMLEntities.Add( new HTMLEntity( "&ordm;", 	"&#186;", 	"�" ) );	// masculine ordinal indicator
			HTMLEntities.Add( new HTMLEntity( "&raquo;", 	"&#187;", 	"�" ) );	// right-pointing double angle quotation HTMLEntities.Add( new HTMLEntity(  mark
			HTMLEntities.Add( new HTMLEntity( "&frac14;", 	"&#188;", 	"�" ) );	// vulgar fraction one quarter
			HTMLEntities.Add( new HTMLEntity( "&frac12;", 	"&#189;", 	"�" ) );	// vulgar fraction one half
			HTMLEntities.Add( new HTMLEntity( "&frac34;", 	"&#190;", 	"�" ) );	// vulgar fraction three quarters
			HTMLEntities.Add( new HTMLEntity( "&iquest;", 	"&#191;", 	"�" ) );	// inverted question mark
			HTMLEntities.Add( new HTMLEntity( "&Agrave;", 	"&#192;", 	"�" ) );	// latin capital letter A with HTMLEntities.Add( new HTMLEntity(  grave
			HTMLEntities.Add( new HTMLEntity( "&Aacute;", 	"&#193;", 	"�" ) );	// latin capital letter A with acute
			HTMLEntities.Add( new HTMLEntity( "&Acirc;", 	"&#194;", 	"�" ) );	// latin capital letter A with circumflex
			HTMLEntities.Add( new HTMLEntity( "&Atilde;", 	"&#195;", 	"�" ) );	// latin capital letter A with tilde
			HTMLEntities.Add( new HTMLEntity( "&Auml;", 	"&#196;", 	"�" ) );	// latin capital letter A with diaeresis
			HTMLEntities.Add( new HTMLEntity( "&Aring;", 	"&#197;", 	"�" ) );	// latin capital letter A with ring above
			HTMLEntities.Add( new HTMLEntity( "&AElig;", 	"&#198;", 	"�" ) );	// latin capital letter AE
			HTMLEntities.Add( new HTMLEntity( "&Ccedil;", 	"&#199;", 	"�" ) );	// latin capital letter C with cedilla
			HTMLEntities.Add( new HTMLEntity( "&Egrave;", 	"&#200;", 	"�" ) );	// latin capital letter E with HTMLEntities.Add( new HTMLEntity(  grave
			HTMLEntities.Add( new HTMLEntity( "&Eacute;", 	"&#201;", 	"�" ) );	// latin capital letter E with acute
			HTMLEntities.Add( new HTMLEntity( "&Ecirc;", 	"&#202;", 	"�" ) );	// latin capital letter E with circumflex
			HTMLEntities.Add( new HTMLEntity( "&Euml;", 	"&#203;", 	"�" ) );	// latin capital letter E with diaeresis
			HTMLEntities.Add( new HTMLEntity( "&Igrave;", 	"&#204;", 	"�" ) );	// latin capital letter I with grave
			HTMLEntities.Add( new HTMLEntity( "&Iacute;", 	"&#205;", 	"�" ) );	// latin capital letter I with acute
			HTMLEntities.Add( new HTMLEntity( "&Icirc;", 	"&#206;", 	"�" ) );	// latin capital letter I with circumflex
			HTMLEntities.Add( new HTMLEntity( "&Iuml;", 	"&#207;", 	"�" ) );	// latin capital letter I with diaeresis
			HTMLEntities.Add( new HTMLEntity( "&ETH;", 		"&#208;", 	"�" ) );	// latin capital letter ETH
			HTMLEntities.Add( new HTMLEntity( "&Ntilde;", 	"&#209;", 	"�" ) );	// latin capital letter N with tilde
			HTMLEntities.Add( new HTMLEntity( "&Ograve;", 	"&#210;", 	"�" ) );	// latin capital letter O with HTMLEntities.Add( new HTMLEntity(  grave
			HTMLEntities.Add( new HTMLEntity( "&Oacute;", 	"&#211;", 	"�" ) );	// latin capital letter O with acute
			HTMLEntities.Add( new HTMLEntity( "&Ocirc;", 	"&#212;", 	"�" ) );	// latin capital letter O with circumflex
			HTMLEntities.Add( new HTMLEntity( "&Otilde;", 	"&#213;", 	"�" ) );	// latin capital letter O with tilde
			HTMLEntities.Add( new HTMLEntity( "&Ouml;", 	"&#214;", 	"�" ) );	// latin capital letter O with diaeresis
			HTMLEntities.Add( new HTMLEntity( "&times;", 	"&#215;", 	"�" ) );	// multiplication sign
			HTMLEntities.Add( new HTMLEntity( "&Oslash;", 	"&#216;", 	"�" ) );	// latin capital letter O with stroke
			HTMLEntities.Add( new HTMLEntity( "&Ugrave;", 	"&#217;", 	"�" ) );	// latin capital letter U with grave
			HTMLEntities.Add( new HTMLEntity( "&Uacute;", 	"&#218;", 	"�" ) );	// latin capital letter U with HTMLEntities.Add( new HTMLEntity(  acute
			HTMLEntities.Add( new HTMLEntity( "&Ucirc;", 	"&#219;", 	"�" ) );	// latin capital letter U with circumflex
			HTMLEntities.Add( new HTMLEntity( "&Uuml;", 	"&#220;", 	"�" ) );	// latin capital letter U with diaeresis
			HTMLEntities.Add( new HTMLEntity( "&Yacute;", 	"&#221;", 	"�" ) );	// latin capital letter Y with HTMLEntities.Add( new HTMLEntity(  acute
			HTMLEntities.Add( new HTMLEntity( "&THORN;", 	"&#222;", 	"�" ) );	// latin capital letter THORN
			HTMLEntities.Add( new HTMLEntity( "&szlig;", 	"&#223;", 	"�" ) );	// latin small letter sharp s
			HTMLEntities.Add( new HTMLEntity( "&agrave;", 	"&#224;", 	"�" ) );	// latin small letter a with grave
			HTMLEntities.Add( new HTMLEntity( "&aacute;", 	"&#225;", 	"�" ) );	// latin small letter a with acute
			HTMLEntities.Add( new HTMLEntity( "&acirc;", 	"&#226;", 	"�" ) );	// latin small letter a with circumflex
			HTMLEntities.Add( new HTMLEntity( "&atilde;", 	"&#227;", 	"�" ) );	// latin small letter a with tilde
			HTMLEntities.Add( new HTMLEntity( "&auml;", 	"&#228;", 	"�" ) );	// latin small letter a with diaeresis
			HTMLEntities.Add( new HTMLEntity( "&aring;", 	"&#229;", 	"�" ) );	// latin small letter a with ring above
			HTMLEntities.Add( new HTMLEntity( "&aelig;", 	"&#230;", 	"�" ) );	// latin small letter ae
			HTMLEntities.Add( new HTMLEntity( "&ccedil;", 	"&#231;", 	"�" ) );	// latin small letter c with cedilla
			HTMLEntities.Add( new HTMLEntity( "&egrave;", 	"&#232;", 	"�" ) );	// latin small letter e with grave
			HTMLEntities.Add( new HTMLEntity( "&eacute;", 	"&#233;", 	"�" ) );	// latin small letter e with acute
			HTMLEntities.Add( new HTMLEntity( "&ecirc;", 	"&#234;", 	"�" ) );	// latin small letter e with circumflex
			HTMLEntities.Add( new HTMLEntity( "&euml;", 	"&#235;", 	"�" ) );	// latin small letter e with diaeresis
			HTMLEntities.Add( new HTMLEntity( "&igrave;", 	"&#236;", 	"�" ) );	// latin small letter i with grave
			HTMLEntities.Add( new HTMLEntity( "&iacute;", 	"&#237;", 	"�" ) );	// latin small letter i with acute
			HTMLEntities.Add( new HTMLEntity( "&icirc;", 	"&#238;", 	"�" ) );	// latin small letter i with circumflex
			HTMLEntities.Add( new HTMLEntity( "&iuml;", 	"&#239;", 	"�" ) );	// latin small letter i with diaeresis
			HTMLEntities.Add( new HTMLEntity( "&eth;", 		"&#240;", 	"�" ) );	// latin small letter eth
			HTMLEntities.Add( new HTMLEntity( "&ntilde;", 	"&#241;", 	"�" ) );	// latin small letter n with tilde
			HTMLEntities.Add( new HTMLEntity( "&ograve;", 	"&#242;", 	"�" ) );	// latin small letter o with grave
			HTMLEntities.Add( new HTMLEntity( "&oacute;", 	"&#243;", 	"�" ) );	// latin small letter o with acute
			HTMLEntities.Add( new HTMLEntity( "&ocirc;", 	"&#244;", 	"�" ) );	// latin small letter o with circumflex
			HTMLEntities.Add( new HTMLEntity( "&otilde;", 	"&#245;", 	"�" ) );	// latin small letter o with tilde
			HTMLEntities.Add( new HTMLEntity( "&ouml;", 	"&#246;", 	"�" ) );	// latin small letter o with diaeresis
			HTMLEntities.Add( new HTMLEntity( "&divide;", 	"&#247;", 	"�" ) );	// division sign
			HTMLEntities.Add( new HTMLEntity( "&oslash;", 	"&#248;", 	"�" ) );	// latin small letter o with stroke
			HTMLEntities.Add( new HTMLEntity( "&ugrave;", 	"&#249;", 	"�" ) );	// latin small letter u with grave
			HTMLEntities.Add( new HTMLEntity( "&uacute;", 	"&#250;", 	"�" ) );	// latin small letter u with acute
			HTMLEntities.Add( new HTMLEntity( "&ucirc;", 	"&#251;", 	"�" ) );	// latin small letter u with circumflex
			HTMLEntities.Add( new HTMLEntity( "&uuml;", 	"&#252;", 	"�" ) );	// latin small letter u with diaeresis
			HTMLEntities.Add( new HTMLEntity( "&yacute;", 	"&#253;", 	"�" ) );	// latin small letter y with acute
			HTMLEntities.Add( new HTMLEntity( "&thorn;", 	"&#254;", 	"�" ) );	// latin small letter thorn
			HTMLEntities.Add( new HTMLEntity( "&yuml;", 	"&#255;", 	"�" ) );	// latin small letter y with diaeresis

			HTMLEntities.Add( new HTMLEntity( "&euro;",		"&#8364;", 	"�" ) );	// euro sign
			HTMLEntities.Add( new HTMLEntity( "&ndash;",	"&#8211;", 	"�" ) );	// sitatstrek
			HTMLEntities.Add( new HTMLEntity( "&tstr;",		"&#8211;", 	"�" ) );	// sitatstrek/tankestrek i TT-meldinger
			HTMLEntities.Add( new HTMLEntity( "&thinsp;",	"", 		" " ) );	// Mykt mellomrom
// 
		}

//		private void BusyTimeoutTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
//		{
//			if( BusyConverting )
//			{
//				WriteToLog( "Warning: The service seems to have hung. Setting BUSY = false.", 1 );
//				BusyConverting = false;
//			}
//		}

		public void GetConfigAttributes()
		{
            
//            ReutersName = Config.Get( "ReutersName" );           
//            ReutersFinansName = Config.Get( "ReutersFinansName" );
//            AFPName = Config.Get( "AFPName" );
//            DPAEngelskName = Config.Get( "DPAEngelskName" );
//            DPATyskName = Config.Get( "DPATyskName" );
//            NordenName = Config.Get( "NordenName" );
//            APName = Config.Get( "APName" );
            ReutersName = BureauConvert.Properties.Settings.Default.ReutersName;
            ReutersFinansName = BureauConvert.Properties.Settings.Default.ReutersFinansName;
            AFPName = BureauConvert.Properties.Settings.Default.AFPName;
            DPAEngelskName = BureauConvert.Properties.Settings.Default.DPAEngelskName;
            DPATyskName = BureauConvert.Properties.Settings.Default.DPATyskName;
            NordenName = BureauConvert.Properties.Settings.Default.NordenName;
            APName = BureauConvert.Properties.Settings.Default.APName;

            // Added for full feed in the nordic feed
            TTFullName = BureauConvert.Properties.Settings.Default.TTFull;
            RitzauFullName = BureauConvert.Properties.Settings.Default.RitzauFull;
            FNBFullName = BureauConvert.Properties.Settings.Default.FNBFull;

            // Getting Fiffus Paths
            FiffusWirePath = BureauConvert.Properties.Settings.Default.FiffusWirePath;

            // Printer Configuration
            PrinterName = BureauConvert.Properties.Settings.Default.PrinterName;

            // Do-stuff
            DoDoc = Convert.ToBoolean(BureauConvert.Properties.Settings.Default.DoDoc);
            DoHTML = Convert.ToBoolean(BureauConvert.Properties.Settings.Default.DoHTML);
            DoPrint = Convert.ToBoolean(BureauConvert.Properties.Settings.Default.DoPrint);
            DoLast12 = Convert.ToBoolean(BureauConvert.Properties.Settings.Default.DoLast12);
            DoAddToFiffus = Convert.ToBoolean(BureauConvert.Properties.Settings.Default.DoAddToFiffus);
            DoArchiveFiles = Convert.ToBoolean(BureauConvert.Properties.Settings.Default.DoArchiveFiles);
            DoDeleteFiles = Convert.ToBoolean(BureauConvert.Properties.Settings.Default.DoDeleteFiles);
            DoWarnViaEmail = Convert.ToBoolean(BureauConvert.Properties.Settings.Default.DoWarnViaEmail);
            ShareWireFiles = Convert.ToBoolean(BureauConvert.Properties.Settings.Default.ShareWireFiles);
            DoAnalyze = Convert.ToBoolean(BureauConvert.Properties.Settings.Default.Analyze);
            DoTest = Convert.ToBoolean(BureauConvert.Properties.Settings.Default.Test);
            DoShowInGUI = Convert.ToBoolean(BureauConvert.Properties.Settings.Default.ShowInGUI);

//            // Added for full feed in the nordic feed
//            TTFullName = Config.Get ("TTFull");
//            RitzauFullName = Config.Get("RitzauFull");
//            FNBFullName = Config.Get("FNBFull");

//            FiffusWirePath = Config.Get( "FiffusWirePath" );
////			FullFiffusWirePath = Config.Get( "FullFiffusWirePath" );
//            PrinterName = Config.Get( "PrinterName" );

//            DoDoc = ( Config.Get( "DoDoc" ) == "true" );
//            DoHTML = ( Config.Get( "DoHTML" ) == "true" );
//            DoPrint = ( Config.Get( "DoPrint" ) == "true" );
//            DoLast12 = ( Config.Get( "DoLast12" ) == "true" );
//            DoAddToFiffus = ( Config.Get( "DoAddToFiffus" ) == "true" );
//            DoArchiveFiles = ( Config.Get( "DoArchiveFiles" ) == "true" );
//            DoDeleteFiles = ( Config.Get( "DoDeleteFiles" ) == "true" );
//            DoWarnViaEmail = ( Config.Get( "DoWarnViaEmail" ) == "true" );

//            ShareWireFiles = ( Config.Get( "ShareWireFiles" ) == "true" );
			
//            DoAnalyze = ( Config.Get( "Analyze" ) == "true" );
//            DoTest = ( Config.Get( "Test" ) == "true" );
//            DoShowInGUI = ( Config.Get( "ShowInGUI" ) == "true" );
		}

        /// <summary>
        /// This method converts the current character
        /// </summary>
        /// <param name="i">The character we are to convert</param>
        /// <returns>Returns the converted character</returns>
		public char Chr(int i)
		{
			//Return the character of the given character value
			return Convert.ToChar(i);
		}

        /// <summary>
        /// This method analyzes the current message
        /// </summary>
        /// <param name="s"></param>
        /// <param name="bureau"></param>
		private void Analyze( Story s, Bureau bureau )
		{
			WriteToLog( "Analyzing...", 3 );

			if( s.IsIgnorable )
			{
				WriteToLog( "IGNORED - The story had a category (\"" +
					s.Category +
					"\") which is being ignored by the system", 3 );

				CurrentStory = s;
				ArchiveAndSaveDocs( bureau, s.ExportFileName, s.FullOriginalPath, true );
			}
			else
			{
				HtmlSource = s.HtmlSource;
				DocSource = s.DocSource;
				FiffusNewLineSource = s.FiffusNewLineSource;

				if( s.IsPrintable )
				{
					PrintCurrent = true;
					PrintSource = s.GeneratePrintSource();
				}
				else
				{
					PrintCurrent = false;
					PrintSource = "";
				}

				CurrentStory = s;
				ArchiveAndSaveDocs( bureau, s.ExportFileName, s.FullOriginalPath );
			}

			if( ! IsService )
			{
				pMainForm.UpdateFormDuringAnalyze( ref s );
			}
		}

		/// <summary>
		/// This function reads the Fiffus File
		/// </summary>
		/// <param name="fullPath">This string contains the full path to the file</param>
		/// <returns></returns>
		public string ReadFiffusFile( string fullPath )
		{
//			if( ! File.Exists( fullPath ) )
//			{
//				FileStream fs = File.Create( fullPath );
//				fs.Close();
//			}
			string txt = "";

			try
			{
				FileStream fs = new FileStream( fullPath, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read );
				StreamReader sr = new StreamReader( fs );

				txt = sr.ReadToEnd();

				sr.Close();
				fs.Close();

			}
			catch( IOException e )
			{
				WriteErrorToLog( "ERROR: Reading file (Fiffus)\r\n" +
					e.Message );
			}

			return txt;
		}

		/// <summary>
		/// This function writes to the FiffusFile
		/// </summary>
		/// <param name="fullPath">This is the path to where the file shall be saved</param>
		/// <param name="txt">This is the content of the file</param>
		public void WriteFiffusFile( string fullPath, string txt )
		{
			try
			{
				FileStream fs;

				if( ShareWireFiles )
				{
					// fs = new FileStream( fullPath, FileMode.Truncate, FileAccess.ReadWrite, FileShare.ReadWrite );
					fs = new FileStream( fullPath, FileMode.Truncate, FileAccess.Write, FileShare.ReadWrite );
				}
				else
				{
					fs = new FileStream( fullPath, FileMode.Truncate, FileAccess.Write, FileShare.None );
				}
            
				// Create a new stream to write to the file
				StreamWriter sw = new StreamWriter( fs );

				// Write a string to the file
				sw.Write( txt );
				
				// Close StreamWriter
				sw.Close();
				fs.Close();
				FiffusWriteCounter = 0;
			}
			catch( IOException e )
			{
				FiffusWriteCounter++;
				WriteToLog( "WARNING: Cannot write to file (Fiffus). I will retry in " + 
					kSleepMs +
					" milliseconds.\r\n",
						1 );

				if( FiffusWriteCounter < kRetryLimit )
				{
					Thread.Sleep( kSleepMs );
					WriteFiffusFile( fullPath, txt );
				}
				else
				{
					WriteErrorToLog( "ERROR: Giving up writing to Fiffus after retrying " + 
						kRetryLimit +
						" times.\r\n" +
						e.Message );

					FiffusWriteCounter = 0;
					throw e;
				}
			}
		}

        /// <summary>
        /// This is the method that prints in the background
        /// </summary>
		public void PrintInBackGround()
		{
			PrintDoc.Print();
		}

        /// <summary>
        /// This method shall check if the printer is ready or not
        /// </summary>
        /// <param name="PrinterName"></param>
        /// <returns></returns>
		public bool IsPrinterReady( string PrinterName )
		{
			ManagementScope scope = new ManagementScope(@"\root\cimv2");
			scope.Connect();
			ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from Win32_Printer");

			string strPrinter = string.Empty;
			foreach(ManagementObject printer in searcher.Get())
			{
				strPrinter = printer["Name"].ToString();
				if(strPrinter.ToLower() == PrinterName.ToLower())
					return printer["PrinterStatus"].ToString().ToLower().Equals("3");
			}
			return false;
		}

        

        /// <summary>
        /// This method shall print out the file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="printerName"></param>
		public void Print( string filePath, string printerName )
		{
			try
			{
				StreamToPrint = new StreamReader( filePath );

				try
				{
                    PrintingPermission perm = new PrintingPermission(System.Security.Permissions.PermissionState.Unrestricted);
                    perm.Level = PrintingPermissionLevel.AllPrinting;

                    // Creating the printDocument object
					PrintDocument pd = new PrintDocument();

                    // Specify the printer to use.
                    pd.PrinterSettings.PrinterName = "\\\\193.69.125.48\\5_IT_DELL5200"; // printerName;
                    

					pd.PrintPage += new PrintPageEventHandler( this.pd_PrintPage );

                    if (pd.PrinterSettings.IsValid != false)
                    {
                        pd.Print();
                    }
				} 
				finally
				{
					StreamToPrint.Close();
				}
			} 
			catch( Exception ex )
			{
				WriteErrorToLog( "ERROR while printing to " +
					printerName +
					".\r\n\r\n" +
					ex.Message + 
					"\r\n" + 
                    ex.StackTrace.ToString(),
					false );

                if (ex.InnerException.Message != "")
                {
                    WriteErrorToLog("InnerException: " +
                        printerName +
                        ".\r\n\r\n" +
                        ex.InnerException.Message +
                        "\r\n",
                        false);
                }
			}

		}

		/// <summary>
        /// The PrintPage event is raised for each page to be printed.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="ev"></param>
		private void pd_PrintPage( object sender, PrintPageEventArgs ev ) 
		{
			float linesPerPage = 0;
			float yPos = 0;
			int count = 0;
			float leftMargin = ev.MarginBounds.Left;
			float topMargin = ev.MarginBounds.Top;
			string line = null;

			// Calculate the number of lines per page.
			linesPerPage = ev.MarginBounds.Height / 
				PrintFont.GetHeight( ev.Graphics );

			// Print each line of the file.
			while( count < linesPerPage && ( ( line = StreamToPrint.ReadLine() ) != null ) ) 
			{
				yPos = topMargin + (count * PrintFont.GetHeight(ev.Graphics));
				ev.Graphics.DrawString(line, PrintFont, Brushes.Black, leftMargin, yPos, new StringFormat());
				count++;
			}

			// If more lines exist, print another page.
			if( line != null )
				ev.HasMorePages = true;
			else
				ev.HasMorePages = false;
		}

        /// <summary>
        /// This method gets the last 12 filenames
        /// </summary>
        /// <returns></returns>
		public string GetLast12Filename()
		{
            return BureauConvert.Properties.Settings.Default.HTMLPath + "\\wire\\index\\siste12.txt";  // .Settings.Default.HTMLPath;
			//return Config.Get( "HTMLPath" ) + "\\wire\\index\\siste12.txt";
			// return Config.Get( "FullFiffusWirePath" );
			
		}

        /// <summary>
        /// This method calls the ArvhiceAndSaveDocs method with the the bool parameter
        /// </summary>
        /// <param name="bureau"></param>
        /// <param name="filename"></param>
        /// <param name="fullSrcPath"></param>
		public void ArchiveAndSaveDocs( Bureau bureau, string filename, string fullSrcPath )
		{
			ArchiveAndSaveDocs( bureau, filename, fullSrcPath, false );
		}

		/// <summary>
		/// This method moves the files to different folders
		/// </summary>
		/// <param name="bureau">The name of the bureau article we are converting</param>
		/// <param name="filename">Filename we are moving and saving</param>
		/// <param name="fullSrcPath">The full path to the directory we are to save</param>
		/// <param name="justDelete">Boolean variable used to state if we shall just delete the file and not move it</param>
		public void ArchiveAndSaveDocs( Bureau bureau, string filename, 
			string fullSrcPath, bool justDelete )
		{
			if( ! justDelete )
			{
				filename = RemoveInvalidCharsInPath( filename ).Replace( " ", "_" );
				WriteToLog( "New serial number: " + BureauConvert.Properties.Cfg.Default.SerialNumber, 3 );
				string relativeHtmlPath = "", htmlFilename = "";

				if( DoHTML )
				{
					WriteToLog( "Saving HTML to: " + bureau.HTMLStoryDirectory, 3 );
					string newsType = "";

					if( CurrentStory.IsSports )
					{
						newsType = "sport";
					}
					else
					{
						newsType = "nyheter";
					}

					if ((bureau.IncomingStoryDirectory.Replace("-", "").Replace(" ", "").ToLower() == "ttfull") ||
						(bureau.IncomingStoryDirectory.Replace("-", "").Replace(" ", "").ToLower() == "rbfull") ||
						(bureau.IncomingStoryDirectory.Replace("-", "").Replace(" ", "").ToLower() == "fnbfull"))
					{
						CurrentStory.Source = bureau.IncomingStoryDirectory.Replace("-", "").Replace(" ", "").ToLower();
					}

					relativeHtmlPath = "\\wire\\data\\" + 
						DateTime.Now.Date.ToString("yyyyMMdd") + 
						"\\" + 
						CurrentStory.Source.Replace( " ", "" ).ToLower() +
						"\\" +
						newsType +
						"\\" +
						filename + 
						".htm";

					htmlFilename = bureau.HTMLStoryDirectory + relativeHtmlPath;

					SaveHTML( htmlFilename );

					if( DoAddToFiffus )
					{
						
						WriteToLog( "Adding story to Fiffus Wire", 3 );

						//	Fil for siste 12 timer
						string last12Filename = GetLast12Filename();
						string last12 = ReadFiffusFile( last12Filename );
						// File.Delete( last12Filename ); 
						WriteFiffusFile( last12Filename, FiffusNewLineSource + relativeHtmlPath.Replace( "\\", "/" ) + "\r\n" + last12 );
  
						// Samlefil for dagens meldinger
						string todayFilename = bureau.HTMLStoryDirectory +
							"\\wire\\index\\" +
							DateTime.Now.Date.ToString("yyyyMMdd") + ".txt";
						string todayFile = ReadFiffusFile( todayFilename );
						// File.Delete( todayFilename ); 
						WriteFiffusFile( todayFilename, FiffusNewLineSource + relativeHtmlPath.Replace( "\\", "/" ) + "\r\n" + todayFile );
					}
				}



				if( DoDoc )
				{
					WriteToLog( "Saving DOC to: " + bureau.DocStoryDirectory, 3 );
					SaveDoc( bureau.DocStoryDirectory + "\\" + filename + ".doc" );
				}

				if( DoPrint && PrintCurrent )
				{
					PrintDocument( ref bureau, filename );
				}

				//FileInfo fi = new FileInfo( filename );
				DirectoryInfo di = new DirectoryInfo( bureau.ArchivedStoryDirectory );

				if( ! di.Exists )
				{
					di.Create();
				}


				if( DoDoc || DoHTML ) // Hmmm, denne sjekken er kanskje un�dvendig da samme sjekk gj�res i CheckAllBureaus()
				{
					if( DoArchiveFiles )
					{
						if( File.Exists( fullSrcPath ) )
						{
							if( File.Exists( bureau.ArchivedStoryDirectory + "\\" + filename ) )
							{
								WriteToLog( "Deleting... (File already exists in archive)", 3 );
								File.Delete( fullSrcPath );
							}
							else
							{
								WriteToLog( "Moving to: " + bureau.ArchivedStoryDirectory, 3 );
								File.Move( fullSrcPath, bureau.ArchivedStoryDirectory + "\\" + filename );
							}
						}
					}
				}
			}
			else
			{
				if( File.Exists( fullSrcPath ) )
				{
					WriteToLog( "Deleting the ignored file...", 3 );
					File.Delete( fullSrcPath );
				}
			}
		}

		public void UpdateLast12( string filename )
		{
			if( ! BusyConverting )
			{
				if( DoLast12 )
				{
					StreamReader sr = new StreamReader( filename );
					string txt = "";
					ArrayList Lines = new ArrayList();
					string input = null;

					DateTime now = DateTime.Now;
					DateTime timeOfStory = new DateTime();

					while( ( input = sr.ReadLine() ) != null )
					{	
						Regex r = new Regex(@"\w+#(\d+.\d+.\d+)#(\d+.\d+)");				
						Match m = r.Match(input);
							
						string dateStr = r.Match(input).Groups[1].Value;
						string timeStr = r.Match(input).Groups[2].Value;

						// dd.MM.yyyy

						int day		= Convert.ToInt32( dateStr.Substring( 0, 2 ) );;
						int month	= Convert.ToInt32( dateStr.Substring( 3, 2 ) );;
						int year	= Convert.ToInt32( dateStr.Substring( 6, dateStr.Length - 6 ) );;

						if( year < 100 )
						{
							year += 2000;
						}

						int hour	= Convert.ToInt32( timeStr.Substring( 0, 2 ) );;
						int minute	= Convert.ToInt32( timeStr.Substring( 3, 2 ) );;

						timeOfStory = new DateTime( year, month, day, hour, minute, 0 );

						TimeSpan span = now - timeOfStory;

						if( span.TotalHours < 12.0 )
						{
							//Lines.Add( input );
							txt += input + "\r\n";
						}
					}
			
					sr.Close();
					//File.Delete( filename );
					WriteFiffusFile( filename, txt );
				}
			}
		}

		public void PrintDocument( ref Bureau bureau, string filename )
		{
			WriteToLog( "Saving document for Print to: " + bureau.PrintStoryDirectory, 2 );
			string printDocumentPath = bureau.PrintStoryDirectory + "\\" + filename + ".doc";
			SavePrintFile( printDocumentPath );

			Print( printDocumentPath, PrinterName );

		}
		public void WriteErrorToLog( string msg )
		{
			WriteErrorToLog( msg, true );
		}

		public void WriteErrorToLog( string msg, bool isFileRelated )
		{
			AccumulatedErrors += msg + "\r\n\r\nThe error occurred during the conversion of the following file :\r\n   " +
				CurrentFileName +
				"\r\n";
			WriteToLog( msg, 2 );

			if( isFileRelated )
			{
				//  	C:\Inetpub\wwwroot\wire\index\siste12.txt") Line 335 + 0x2e bytes	C#

				FileInfo fi = new FileInfo( CurrentFileName );
				
				if( ! Directory.Exists( CurrentBureau.FailedDirectory ) )
				{
					Directory.CreateDirectory( CurrentBureau.FailedDirectory );
				}
				if( CurrentFileName != "" && File.Exists( CurrentFileName ) )
				{
					File.Copy( CurrentFileName, CurrentBureau.FailedDirectory + "\\" + fi.Name, true );
					PendingFilesForDeletion.Add( CurrentFileName );
					//				CurrentFileName = "";
				}
			}
		}

		private void DeleteFailedFiles()
		{
			CurrentFileName = "";

			foreach( string s in PendingFilesForDeletion )
			{
				if( File.Exists( s ) )
				{
					WriteToLog( "Deleting Pending failed file " + s, 1 );
					File.Delete( s );
				}
			}
		}
			
		public void WriteToLog( string msg, int indentLevel )
		{
			string indent = "";
			const int indentFactor = 3;

			for( int i = 0; i < ( indentLevel * indentFactor ); i++ )
			{
				indent += " ";
			}

//			if( LogFile == null )
//			{
				PrepareLogFile();
//			}

			LogFile.WriteLine( DateTime.Now.ToString() +
				": " +
				indent +
				msg );

			CloseLogFile();
		}

        /// <summary>
        /// This method checks all Bureas for new content
        /// </summary>
		public void CheckAllBureaus()
		{
			if( ! BusyConverting )
			{
				try
				{
					BusyConverting = true;
					LastActionTime = DateTime.Now;

					if( DoDoc || DoHTML )
					{
						// PrepareLogFile();
						WriteToLog( "Starting sequence for checking incoming bureau messages", 0 );
			
						Bureaus = new ArrayList();
						Bureaus.Add( new Bureau( ReutersName ) );
						Bureaus.Add( new Bureau( AFPName ) );
						Bureaus.Add( new Bureau( DPAEngelskName ) );
						Bureaus.Add( new Bureau( DPATyskName ) );
						Bureaus.Add( new Bureau( NordenName ) );
						Bureaus.Add( new Bureau( APName ) );
						Bureaus.Add( new Bureau( ReutersFinansName ) );

						// 15.03.2010: Added full feeds from the nordic bureaus
						
						Bureaus.Add (new Bureau (TTFullName));
						Bureaus.Add (new Bureau (RitzauFullName));
						Bureaus.Add (new Bureau (FNBFullName));

						int t = 0;

						foreach( Bureau bureau in Bureaus )
						{
							CurrentBureau = bureau;
							WriteToLog( "Bureau: " + bureau.Name, 1 );

							Story s = new Story();
							bool analyze = DoAnalyze;
							DirectoryInfo di = new DirectoryInfo( bureau.IncomingStoryDirectory );

							foreach( FileInfo file in di.GetFiles() )
							{
								LastActionTime = DateTime.Now;

								if( file.Length > 0 )
								{
									WriteToLog( "Incoming file: " + file.FullName, 2 );
									CurrentFileName = file.FullName;

									// If there are new feeds from bureaus you have to add them in this section
									// Remember that you have to create links to the BCService in the document

									try
									{
										if( bureau.Name == ReutersName )
										{
											ReutersStory rs = new ReutersStory( file.FullName, analyze, DoTest, ref Conf );
											s = rs;
										}
										if( bureau.Name == ReutersFinansName )
										{
											ReutersFinansStory rs = new ReutersFinansStory( file.FullName, analyze, DoTest, ref Conf );
											s = rs;
										}
										else if( bureau.Name == AFPName )
										{
											AFPStory rs = new AFPStory( file.FullName, analyze, DoTest, ref Conf );
											s = rs;
										}
										else if( bureau.Name == DPAEngelskName )
										{
											DPAEngStory rs = new DPAEngStory( file.FullName, analyze, DoTest, ref Conf );
											s = rs;
										}
										else if( bureau.Name == DPATyskName )
										{
											DPATyskStory rs = new DPATyskStory( file.FullName, analyze, DoTest, ref Conf );
											s = rs;
										}
										else if( bureau.Name == NordenName )
										{
											NordenStory rs = new NordenStory( file.FullName, analyze, DoTest, ref Conf );
											s = rs;
										}
										else if( bureau.Name == APName )
										{
											APStory rs = new APStory( file.FullName, analyze, DoTest, ref Conf );
											s = rs;
										}

										else if (bureau.Name == TTFullName) 
										{
											TTFullStory rs = new TTFullStory ( file.FullName, analyze, DoTest, ref Conf );
											NordicFull = "ttfull";
											s = rs;
										}

										else if (bureau.Name == RitzauFullName) 
										{
											RitzauFullStory rs = new RitzauFullStory ( file.FullName, analyze, DoTest, ref Conf );
											NordicFull = "rbfull";
											s = rs;
										}

										else if (bureau.Name == FNBFullName) 
										{
											FNBFullStory rs = new FNBFullStory ( file.FullName, analyze, DoTest, ref Conf );
											NordicFull = "fnbfull";
											s = rs;
										}

										t++;

										if( DoShowInGUI )
										{
											if( ! IsService )
											{
												pMainForm.UpdateFormBeforeAnalyze( ref s, t );
											}
										}

										if( analyze )
										{
											Analyze( s, bureau );
										}
									}
									catch( Exception e )
									{
										WriteErrorToLog( "ERROR in " + 
											e.Source + 
											".\r\n  Error message: " 
											+ e.Message +
											"\r\n" + 
											e.StackTrace + 
											"\r\n" );
									}
									finally
									{
										CurrentFileName = "";
									}
								}
								else
								{
									WriteToLog( "WARNING: File empty. Skipping this file\r\n", 1 );

									if( File.Exists( file.FullName ) )
									{
										if( DoArchiveFiles )
										{
											WriteToLog( "Copying to: " + bureau.ArchivedStoryDirectory, 3 );
											File.Copy( file.FullName, bureau.ArchivedStoryDirectory + "\\" + file.Name, true );
										}
                                        if (DoDeleteFiles)
                                        {
                                            WriteToLog("Deleting...", 3);
                                            File.Delete(file.FullName);
                                        }
									}
								}
							}
						}
			
						Bureaus.Clear();
					}
				}
				catch( Exception e )
				{
					WriteErrorToLog( "ERROR outside file iteration. The error occurred in " + 
						e.Source + 
						".\r\n  Error message: " + 
						e.Message +
						"\r\n" );
				}
				finally
				{
					BusyConverting = false;
					DeleteFailedFiles();
					WriteToLog( "Sequence for checking incoming bureau messages FINISHED\r\n", 0 );
				}
			}
			else
			{
				TimeSpan ts = DateTime.Now - LastActionTime;
				WriteToLog( "WARNING: Cannot start conversion process because another conversion process seems to be running.\r\n", 1 );

				if( ts.TotalMilliseconds > BusyTimeoutMSeconds )
				{
					BusyConverting = false;
					WriteToLog( "WARNING: BusyTimeout has occurred. Resetting BusyConverting to \"false\".\r\n", 1 );
				}
			}
		}

        /// <summary>
        /// This method removes invalid characters in path
        /// </summary>
        /// <param name="path">string containing the path</param>
        /// <returns>The cleaned string</returns>
		public string RemoveInvalidCharsInPath( string path )
		{
			path = Regex.Replace(path, @"[?:\/*""<>|]", "");
			return path;
		}

        /// <summary>
        /// This method calls the save method with HTMLsource as content string
        /// </summary>
        /// <param name="filename"></param>
		public void SaveHTML( string filename )
		{
			Save( filename, HtmlSource );
		}

        /// <summary>
        /// This method calls the save method 
        /// </summary>
        /// <param name="filename"></param>
		public void SaveDoc( string filename )
		{
			Save( filename, DocSource );
		}

		/// <summary>
		/// This method shall create the Print file.
		/// </summary>
		/// <param name="filename"></param>
		/// 
		/// NOTE:
		/// The different byte stuff is here just because we tried to fix the 
		/// odd characters bug when you print out the file.
		/// The odd character bug is not related to this program, but a serviced script
		/// that runs on the printer server (delhi)
		/// 19.01.2010: Trond Hus� (thu@ntb.no)
		/// 

		public void SavePrintFile( string filename )
		{
			// Save( filename, PrintSource );

            DirectoryInfo di = new DirectoryInfo(filename);

            if (!di.Parent.Exists)
            {
                di.Parent.Create();
            }

            // Making sure that PrintSource is a string
            string ps = PrintSource.ToString();



            StreamWriter sw = new StreamWriter(filename, false, System.Text.Encoding.GetEncoding("ISO-8859-15"));
            sw.Write(PrintSource);
            sw.Close();
		}

        /// <summary>
        /// This method is used to save the file
        /// </summary>
        /// <param name="filename">Name of the file to be saved</param>
        /// <param name="content">The content we are storing in the file</param>
		public void Save( string filename, string content )
		{
			DirectoryInfo di = new DirectoryInfo( filename );

			if( ! di.Parent.Exists )
			{
				di.Parent.Create();
			}

			
			StreamWriter sw = new StreamWriter( filename, false, System.Text.Encoding.Default );
			sw.Write( content );
			sw.Close();
		}

        /// <summary>
        /// This method closes the LogFile
        /// </summary>
		public void CloseLogFile()
		{
			if( LogFile != null )
			{
				LogFile.Close();
			}
		}

        /// <summary>
        /// This method prepares the log-file for the application
        /// </summary>
		public void PrepareLogFile()
		{
			CloseLogFile();

			string logFileName = "ConDis_" + 
				DateTime.Now.Date.Year.ToString() + "-" +
				Utilities.FormatTimeElement( DateTime.Now.Date.Month ) + "-" +
				Utilities.FormatTimeElement( DateTime.Now.Date.Day ) + 
				".log";

            FullLogPath = BureauConvert.Properties.Settings.Default.LogPath + "\\" + logFileName; // Config.Get("LogPath") + "\\" + logFileName;

			DirectoryInfo d = new DirectoryInfo( FullLogPath );

			if( ! d.Parent.Exists )
			{
				d.Parent.Create();
			}

            LogFile = new StreamWriter( FullLogPath, true );
		}

        /// <summary>
        /// This method is run everytime ErrorIntervalTimer Elapsed ticks in.
        /// </summary>
        /// <param name="interval">Double value which decides when to tick in</param>
		public void ErrorIntervalTimer_Elapsed( double interval )
		{
			if( ! BusyConverting )
			{
				if( AccumulatedErrors != "" )
				{
					if( DoWarnViaEmail )
					{
						MailMessage msg = new MailMessage();
                        // msg.To.Add(Config.Get("MailTo"));
                        msg.To.Add(BureauConvert.Properties.Settings.Default.MailTo);
                        // msg.CC.Add(Config.Get( "MailCc" ));
                        msg.CC.Add(BureauConvert.Properties.Settings.Default.MailCc);
						// msg.From = new MailAddress(Config.Get( "MailFrom" ));
                        msg.From = new MailAddress(BureauConvert.Properties.Settings.Default.MailFrom);
						// msg.Subject = Config.Get( "MailSubject" );
                        msg.Subject = BureauConvert.Properties.Settings.Default.MailSubject;
                        msg.IsBodyHtml = false;
						//msg.Attachments.Add

						msg.Body = "The following errors have occured during the last " +
							Convert.ToString( interval / 1000 ) + 
							" seconds:\r\n\r\n" +
							AccumulatedErrors;

                        // SmtpClient smtp = new SmtpClient(Config.Get("SmtpServer"));
                        SmtpClient smtp = new SmtpClient(BureauConvert.Properties.Settings.Default.SMTPServer);
                        smtp.Send(msg);
						
					}

					AccumulatedErrors = "";
				}
			}
		}
	}
}
