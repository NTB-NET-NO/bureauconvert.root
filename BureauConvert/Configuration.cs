using System;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Reflection;
using System.Configuration;

namespace ConDis
{
	/// <summary>
	/// This class handles everything related to the config.xml-file
	/// 
	/// </summary>
	public class Configuration //: System.Xml.Serialization.IXmlSerializable
	{
		public int SerialNumber = 0;
		public string FileName = "";

		public Configuration()
		{

            FileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\" +
                BureauConvert.Properties.Settings.Default.ConfigFileName;
                
                
                // Config.Get( "ConfigFileName" );

			Uri uri = new Uri( FileName );
			FileName = uri.LocalPath;

			if( ! File.Exists( FileName ) )
			{
				Serialize();
			}
		}

		public void Serialize() 
		{
			XmlSerializer serializer = new XmlSerializer( typeof( Configuration ) );

			Stream fs = new FileStream( FileName, FileMode.OpenOrCreate );
			XmlWriter writer = new XmlTextWriter( fs, new UTF8Encoding() );

			serializer.Serialize( writer, this );
			writer.Close();
		}

		public void Deserialize()
		{   
			XmlSerializer serializer = new XmlSerializer( typeof( Configuration ) );

			FileStream fs = new FileStream( FileName, FileMode.Open );
			XmlReader reader = new XmlTextReader( fs );
        
			Configuration tmpConf = new Configuration();
			tmpConf = ( Configuration )serializer.Deserialize( reader );

			SerialNumber = tmpConf.SerialNumber;

			reader.Close();

		}

	}
}
